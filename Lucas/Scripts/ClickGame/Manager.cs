using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{
    // Start is called before the first frame update
    public Text ClicksTotalText;
    public CountdownTimer countdowntimer;
    public static float ScoreClick;
    public Button ClickButton;
  
    bool inGame;
    private AudioSource typing_sound;

    private float timer;

    void Start(){
        typing_sound = GetComponent<AudioSource>();
        inGame = true;
    }

    void Update(){
       if (inGame)
       {
           UpdateInGame();
       }      
    }

    void UpdateInGame()
    {
         timer -= Time.deltaTime;
         if(timer<=0){
            typing_sound.Stop();
            timer = 0.0f;
        }
        else{
            if(!typing_sound.isPlaying && countdowntimer.currentTime > 0){
                typing_sound.Play();
            }
        }
        if (ScoreClick == ControlDifficulty.clickObjective)
        {
            timer = 0.0f;
            countdowntimer.currentTime = 0.0f;
        }
        if (countdowntimer.currentTime == 0.0f)
        {
            GameOver();
        }
    }

    void GameOver()
    {
        inGame = false;
        ScoreClick = ControlDifficulty.clickcoeff*ScoreClick;
        print(ScoreClick);
        Debug.Log("hey its the end");
        RealGameManager.instance.Change_to_UI();
    }

    public void AddClicks()
    {
        ClickButton.onClick.AddListener(KeyboardSound);
         if (countdowntimer.currentTime > 0 && ScoreClick < ControlDifficulty.clickObjective)
        {
            ScoreClick++;
            ClicksTotalText.text = ScoreClick.ToString("0");
        }
        else{
            typing_sound.Stop();
        }
    }

    private void KeyboardSound()
    {
        timer = 0.4f;
    }
}
