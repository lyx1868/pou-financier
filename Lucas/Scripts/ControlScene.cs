using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlScene : MonoBehaviour
{
    public void NextSceneClickGame()
    {
        SceneManager.LoadScene("GameClick", LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync("MenuClick");
    }

    public void NextSceneDropGame()
    {
        SceneManager.LoadSceneAsync("GameDrop", LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync("MenuDrop");
    }

    public void NextSceneMemoryGame()
    {
        SceneManager.LoadSceneAsync("GameMemory", LoadSceneMode.Additive);
        SceneManager.UnloadSceneAsync("MenuMemory");
    }

}
