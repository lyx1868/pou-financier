using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    public float currentTime;
    [SerializeField] Text countdownText;

    // Start is called before the first frame update
    void Start()
    {
        if (ControlDifficulty.game == 1)
        {
            currentTime = ControlDifficulty.clickTime;
        }

        else if (ControlDifficulty.game == 2)
        {
            currentTime = ControlDifficulty.dropTime;
        }

    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= Time.deltaTime;
        countdownText.text = currentTime.ToString("0");

        if (currentTime <= 0)
        {
            currentTime = 0;
        }

        if (currentTime <= 3)
        {
            countdownText.color = Color.red;
        }
    }
}
