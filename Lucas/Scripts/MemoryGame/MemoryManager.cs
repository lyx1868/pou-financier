using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MemoryManager : MonoBehaviour
{
    public SpriteRenderer[] colours;
    public AudioSource[] buttonSounds;
    private int colourSelect;

    public float stayLit;
    private float stayLitCounter;

    private float waitBetweenLights = 0.25f;
    private float waitBetweenCounter;

    private bool shouldBeLit;
    private bool shouldBeDark;

    public List<int> activeSequence;
    private int positionInSequence;

    private bool gameActive;
    private int inputInSequence;

    public AudioSource correct;
    public AudioSource incorrect;

    public Text scoreText;

    public static float MemoryScore; 


    // Start is called before the first frame update
    void Start()
    {
        scoreText.text = "Score: 0";

        activeSequence.Clear();

        positionInSequence = 0;
        inputInSequence = 0;

        colourSelect = Random.Range(0, colours.Length);

        activeSequence.Add(colourSelect);

        colours[activeSequence[positionInSequence]].color = new Color(colours[activeSequence[positionInSequence]].color.r, colours[activeSequence[positionInSequence]].color.g, colours[activeSequence[positionInSequence]].color.b, 1f);

        buttonSounds[activeSequence[positionInSequence]].Play();

        stayLitCounter = stayLit;

        shouldBeLit = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(shouldBeLit)
        {
            stayLitCounter -= Time.deltaTime;

            if(stayLitCounter < 0)
            {
                colours[activeSequence[positionInSequence]].color = new Color(colours[activeSequence[positionInSequence]].color.r, colours[activeSequence[positionInSequence]].color.g, colours[activeSequence[positionInSequence]].color.b, 0.5f);
                
                buttonSounds[activeSequence[positionInSequence]].Stop();

                shouldBeLit = false;

                shouldBeDark = true;
                waitBetweenCounter = waitBetweenLights;

                positionInSequence++;
            }

        }

        if(shouldBeDark)
        {
            waitBetweenCounter -= Time.deltaTime; 

            if(positionInSequence >= activeSequence.Count)
            {
                shouldBeDark = false;
                gameActive = true;
            } 
            
            else
            {
                if(waitBetweenCounter < 0)
                {
                    colours[activeSequence[positionInSequence]].color = new Color(colours[activeSequence[positionInSequence]].color.r, colours[activeSequence[positionInSequence]].color.g, colours[activeSequence[positionInSequence]].color.b, 1f);

                    buttonSounds[activeSequence[positionInSequence]].Play();
                    
                    stayLitCounter = stayLit;

                    shouldBeLit = true;
                    shouldBeDark = false;
                }
            }
        }
    }

    public void ColourPressed(int whichButton){
        if (gameActive)
        {
            if (activeSequence[inputInSequence] == whichButton)
            {
                inputInSequence++;

                if(inputInSequence >= activeSequence.Count)
                {
                    scoreText.text = "Score: " + activeSequence.Count;

                    if (activeSequence.Count >= ControlDifficulty.memoryObjective)
                    {
                        GameOver();
                    }
                    else
                    {
                        positionInSequence = 0;
                        inputInSequence = 0;

                        colourSelect = Random.Range(0, colours.Length);

                        activeSequence.Add(colourSelect);

                        colours[activeSequence[positionInSequence]].color = new Color(colours[activeSequence[positionInSequence]].color.r, colours[activeSequence[positionInSequence]].color.g, colours[activeSequence[positionInSequence]].color.b, 1f);

                        buttonSounds[activeSequence[positionInSequence]].Play();
                        
                        stayLitCounter = stayLit;

                        shouldBeLit = true;
                        gameActive = false;
                        correct.Play();
                    }
                }
            }
            else 
            {
                incorrect.Play();
                GameOver();
            }
        }
    }

    public void GameOver()
    {
        gameActive = false; 
        MemoryScore = ControlDifficulty.memorycoeff * activeSequence.Count;
        print(MemoryScore);
        Debug.Log("hey its the end");
        RealGameManager.instance.Change_to_UI();
    }
}
