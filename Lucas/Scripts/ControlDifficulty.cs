using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDifficulty : MonoBehaviour
{
    public static int game;
    public static int difficulty;

    public static float clickTime;
    public static float clickObjective;
    public static float clickcoeff;

    public static float dropTime;
    public static float dropspawntime;
    public static float dropObjective;
    public static float dropcoeff;

    public static float memoryObjective;
    public static float memorycoeff;

    public void ClickEasy()
    {
        game = 1;
        difficulty = 1;

        clickTime = 30f;
        clickObjective = 75;
        clickcoeff = (1f/75f);

    }

    public void ClickMedium()
    {
        game = 1;
        difficulty = 2;

        clickTime = 20f;
        clickObjective = 90;
        clickcoeff = (1f/90f);
    }

    public void ClickHard()
    {
        game = 1;
        difficulty = 3;

        clickTime = 15f;
        clickObjective = 100;
        clickcoeff = (1f/100f);
    }

    public void DropEasy()
    {
        game = 2;
        difficulty = 1;

        dropTime = 30f;
        dropspawntime = 0.8f;
        dropObjective = 5;
        dropcoeff = (1f/5f);
    }

    public void DropMedium()
    {
        game = 2;
        difficulty = 2;

        dropTime = 30f;
        dropspawntime = 0.7f;
        dropObjective = 10;
        dropcoeff = (1f/10f);
    }

    public void DropHard()
    {
        game = 2;
        difficulty = 3;
        
        dropTime = 30f;
        dropspawntime = 0.6f;
        dropObjective = 20;
        dropcoeff = (1f/20f);
    }

    public void MemoryEasy()
    {
        game = 3;
        difficulty = 1;

        memoryObjective = 5;
        memorycoeff = (1f/5f);
    }

    public void MemoryMedium()
    {
        game = 3;
        difficulty = 2;

        memoryObjective = 10;
        memorycoeff = (1f/10f);
    }

    public void MemoryHard()
    {
        game = 3;
        difficulty = 3;

        memoryObjective = 15;
        memorycoeff = (1f/15f);
    }

    

}