using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collector : MonoBehaviour
{
    public Text scoreText;
    private int score;

    void Update()
    {
        scoreText.text = score.ToString();
    }
    void OnTriggerEnter2D(Collider2D target)
    {
        if (ControlDifficulty.dropObjective > ScoreController.score)
        {
            if (target.tag == "Bomb" || target.tag =="Objects")
            {
                Destroy(target.gameObject);
                if (target.tag == "Objects" && ScoreController.score > 0)
                {
                    ScoreController.score--;
                }
            }
        }
    }
}
