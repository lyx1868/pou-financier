using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChestMovement : MonoBehaviour
{
    private Rigidbody2D myBody;
    public float speed, xBound;

    void Start()
    {
        myBody = GetComponent<Rigidbody2D>();
        MyButtonLeft.buttonPressed = false;
        MyButtonRight.buttonPressed = false;
    }

    void FixedUpdate()
    {
        //Input.gyro
        //Input.touches[0]
        //Vector3 dest;
        //Vector3.Lerp(trasnform.position, dest, Time.deltaTime);

        if (MyButtonRight.buttonPressed == true)
        {
            myBody.velocity = Vector2.right * speed;
        }
        else if (MyButtonLeft.buttonPressed == true)
        {
            myBody.velocity = Vector2.left * speed;
        }
        else
        {
            myBody.velocity = Vector2.zero;
        }

        transform.position = new Vector2(Mathf.Clamp(transform.position.x, -xBound, xBound), transform.position.y);
        
    }
}
