using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] objects;
    public GameObject bomb;

    public float xBounds, yBound;
    private float waiting;
    public float timer;
    public CountdownTimer countdowntimer;
    //public EventsManager eventsMgr;


    void Start()
    {
        waiting = ControlDifficulty.dropspawntime;
        StartCoroutine(SpawnRandomGameObject());
        //eventsMgr.GameReady();
    }

    void Update()
    {
        if (ControlDifficulty.dropObjective == ScoreController.score)
        {
            countdowntimer.currentTime = 0f;
        }
    }
    IEnumerator SpawnRandomGameObject()
    {
        yield return new WaitForSeconds(waiting);
        float randomType = Random.Range(0.0f, 1.0f);

        if (countdowntimer.currentTime > 0) 
        {
            if (ControlDifficulty.dropObjective > ScoreController.score)
            {

            if(randomType < 0.6f)
            {
                int randomObject = Random.Range(0, objects.Length);

                Instantiate(objects[randomObject], new Vector2(Random.Range(-xBounds, xBounds), yBound), Quaternion.identity);
            }

            else
            {
                Instantiate(bomb, new Vector2(Random.Range(-xBounds, xBounds), yBound), Quaternion.identity);
            }

            StartCoroutine(SpawnRandomGameObject());

            }
            
        }
        
    }
}
