using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ScoreController : MonoBehaviour
{
    public Text scoreText;
    public static float score;
    public static float ScoreDrop;

    public AudioSource bombAudio;
    public AudioSource scoreAudio;

    bool InGame;


    void Start()
    {
        score = 0;
        InGame = true;
    }

    void Update()
    {
        if (InGame)
        {
            scoreText.text = score.ToString();
            if (ControlDifficulty.dropObjective == ScoreController.score)
            {
                Game_Over();
            }
        }
    }

    void OnTriggerEnter2D(Collider2D target)
    {
        if (ControlDifficulty.dropObjective > ScoreController.score)
        {
            if(target.tag == "Bomb")
            {
                bombAudio.Play();
                Game_Over();
            }
        }
    }

    void Game_Over()
    {
        InGame = false;
        ScoreDrop = ControlDifficulty.dropcoeff * score;
        print(ScoreDrop);
        Debug.Log("hey its the end");
        RealGameManager.instance.Change_to_UI();
    }

    void OnTriggerExit2D(Collider2D target)
    {
        if (ControlDifficulty.dropObjective > ScoreController.score)
        {
            if (target.tag == "Objects")
            {
                Destroy(target.gameObject);
                score++;
                scoreAudio.Play();
            }
        }
    }
}
