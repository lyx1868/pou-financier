using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Happiness : MonoBehaviour
{
    public int curHappiness = 0;
    public int maxHappiness = 100;

    public HappinessBar happinessBar;

    void Start()
    {
        curHappiness = maxHappiness;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            DamagePlayer(5);
        }
    }

    public void DamagePlayer(int damage)
    {
        curHappiness -= damage;

        happinessBar.SetHappiness(curHappiness);
    }
}
