using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class manager_testUI : MonoBehaviour
{
    private int week;
    private int month;
    public bool week_update_requirement;


    // Start is called before the first frame update
    void Start()
    {
        week = 1;
        month = 1;
        week_update_requirement = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(week_update_requirement == true){
            if(week == 4){
                week = 0;
                if(month == 6){
                    month = 0;
                }
                month = month + 1;
            }
            week = week + 1;
            if(week == 1){
                Menu_Button2.instance.new_month(month,0,0,0,0,0);
            }
            else{
                Menu_Button2.instance.new_week(month,week,12,0,0,0,0);
            }
            week_update_requirement = false;
        }
        
    }
}
