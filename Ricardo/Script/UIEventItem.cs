using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIEventItem : MonoBehaviour
{
    
    public RealGameManager.EventsSent eventSent;

    private List<EventsManager.EventsSent> events_;
    public Text txtEvent;
    public Text txtOption1;
    public Text txtOption2;

    public void SetEvent (RealGameManager.EventsSent e)
    {
        eventSent = e;
        

        
        txtEvent.text = "eventSent.event_text";
        txtOption1.text = eventSent.answer_1;
        txtOption2.text = eventSent.answer_2;
        print(eventSent.event_text);
    }



}
