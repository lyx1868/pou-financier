using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu_Button2 : MonoBehaviour
{
    public GameObject MenuPanel;
    public GameObject LevelSelectPanel;
    public GameObject UIscore;
    public GameObject UIevent;
    public GameObject UIresults;
    public GameObject UIendgame;
    public GameObject UIhome;

    public Text month_LevelSelectPanel;
    public Text Budget_LevelSelectPanel;
    private string budget_LSP_st;
    private string month_LSP;
    public HappinessBar new_month_playerHappiness;
    public HealthBar new_month_playerHealth;

    public Text month_UIScore;
    public Text Budget_UIScore;
    public Text Week_UIScore;
    private string budgetUIScore_LSP_st;
    private string monthUIScore_LSP;
    private string weekUIScore_LSP;
    public HappinessBar new_week_playerHappiness;
    public HealthBar new_week_playerHealth;

    //---------------------------------------------****SINGLETON****------------------------------------------//

    private static Menu_Button2 _instance;
    public static Menu_Button2 instance
    {
        get
        {
            if (_instance == null){
                _instance = GameObject.FindObjectOfType<Menu_Button2>();
            }
            return _instance;
        }
    }

    //dans un autre scritp on écrit Menu_Button2.instance
    
    //----------------------------------------------------------------------------------------------------------//

    // Start is called before the first frame update
    void Start()
    {
        MenuPanel.SetActive(true);
        LevelSelectPanel.SetActive(false);
        UIscore.SetActive(false); 
        UIevent.SetActive(false); 
        UIresults.SetActive(false); 
        UIendgame.SetActive(false);
        UIhome.SetActive(false);
    }

    public void ShowLevelPanel()
    {
        MenuPanel.SetActive(false);
        LevelSelectPanel.SetActive(false);
        UIscore.SetActive(false);
        UIevent.SetActive(false);
        UIresults.SetActive(false);
        UIendgame.SetActive(false);
        UIhome.SetActive(true);
    }

    public void ShowMenuPanel()
    {
        MenuPanel.SetActive(true);
        LevelSelectPanel.SetActive(false);
        UIscore.SetActive(false);
        UIevent.SetActive(false);
        UIresults.SetActive(false);
        UIendgame.SetActive(false);
        UIhome.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void new_month(int month, int happy, int health, int energy, int SS, int budget){
        if(month == 1){
            month_LSP = "Jan";
        }
        else{
            if(month == 2){
                month_LSP = "Feb";
            }
            else{
                if(month == 3){
                    month_LSP = "Mar";
                }
                else{
                    if(month == 4){
                        month_LSP = "Apr";
                    }
                    else{
                        if(month == 5){
                            month_LSP = "May";
                        }
                        else{
                            month_LSP = "Jun";
                        }
                    }
                }
            }
        }
        month_LevelSelectPanel.text = month_LSP;
        new_month_playerHappiness.SetHappiness(happy);
        new_month_playerHealth.SetHealth(health);
        budget_LSP_st = budget.ToString();
        Budget_LevelSelectPanel.text = budget_LSP_st;


    }

    public void new_week(int month, int week, int happy, int health, int energy, int SS, int budget){
        if(month == 1){
            monthUIScore_LSP = "Jan";
        }
        else{
            if(month == 2){
                monthUIScore_LSP = "Feb";
            }
            else{
                if(month == 3){
                    monthUIScore_LSP = "Mar";
                }
                else{
                    if(month == 4){
                        monthUIScore_LSP = "Apr";
                    }
                    else{
                        if(month == 5){
                            monthUIScore_LSP = "May";
                        }
                        else{
                            monthUIScore_LSP = "Jun";
                        }
                    }
                }
            }
        }
        month_UIScore.text = monthUIScore_LSP;
        weekUIScore_LSP = week.ToString();
        Week_UIScore.text = weekUIScore_LSP;
        new_week_playerHappiness.SetHappiness(happy);
        new_week_playerHealth.SetHealth(health);
        budgetUIScore_LSP_st = budget.ToString();
        Budget_UIScore.text = budgetUIScore_LSP_st;


    }
        
}

