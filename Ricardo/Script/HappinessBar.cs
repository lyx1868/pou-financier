using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HappinessBar : MonoBehaviour
{
    public Slider happinessBar;
    //public Happiness playerHappiness;

    private void Start()
    {
        //playerHappiness = GameObject.FindGameObjectWithTag("Player").GetComponent<Happiness>();
        happinessBar = GetComponent<Slider>();
        happinessBar.maxValue = 100;
    }

    public void SetHappiness(int hpp)
    {
        happinessBar.value = hpp;
    }
}
