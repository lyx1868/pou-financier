using System.Collections;
using System.Collections.Generic;
using UnityEngine;




//hello, it's a script tell you how to communicate with game manager



public class Test_game_manerger : MonoBehaviour
{
    //                                      ################ building connection################

    //introduce gamemanager, and drag object "RealGameManager" in unity to set connection
    public RealGameManager realGameManager;

    private int health_value;


    //                                      ##############recieve value and send value to GameManager

    private void Start()
    {
        health_value = realGameManager.Health; //recieve value from GameManager
        Debug.Log("welcom!");
        Debug.Log(health_value + "   is the health value");

        health_value = health_value + 10;

        realGameManager.Health = health_value; //send value to GameManager



        //                                          ############# Change scene#################

        // Don't forget to add the scene into buildsetting
        // Check RealGameManager and serch for scene changing function
        Invoke("change_scene", 3);

    }

    private void change_scene()
    {
        realGameManager.Change_to_mini_game();
    }
}


