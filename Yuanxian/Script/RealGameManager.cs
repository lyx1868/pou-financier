using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RealGameManager : MonoBehaviour
{

    /// ###############################################################################################
    /// ##########################          Variables platform         ###################################
    /// ###############################################################################################
    /// Attention: GameManager only provide a platform for you to send variables and recieve variable, 
    /// GameManager's job is only making these variables public.
    //                                   ##############    Global   variable ######################
    public int Health ;
    public int Energy ;
    public int Social_health;
    public int Happiness;

    //                                  ##############  Variables communicated with   UI   ######################
    public int Week;
    public int Month;                   
    public float Budget_real_time;
    private List<EventsManager.EventsSent> events_;
    public bool Game_is_end;
    //public List<int> _player;


    //                                  ##############  Variables communicated with   GameManager   ######################
    public float mini_game_score;
    //informs that the player has finished to make its choices
    public string mini_game_effort;
    //player's score on the mini game

    //                          ###########     Variables communicated with EventManager    #################


    //Variables to exchange information with game manager:
    //THIS VARIABLES ARE GIVEN BY THE GAME MANAGER:
    //asks for new events
    public int mini_game_score_int;
    //public bool events_requirement = false;
    //informs that the player has finished the mini-game
    public bool mini_game_info_ready = false;
    //informs that the player has finished to make its choices
    //public bool events_choices_ready = false;
    //player's choices for each question: 0 -> answer 1 was chosen, 1 -> answer 2 was chosen
    //PAY ATTENTION TO THIS VARIABLE !!!!!!!!!!!!!!!!!
    public List<int> choices_player = new List<int>();
    //THIS VARIABLES ARE GIVEN TO THE GAME MANAGER:
    //Informs that the events are ready to be sent
    //public bool events_selection_ready = false;  
    //Events selected for the week
    //public List<EventsSent> Events_Of_The_Week;
    //How the budget is going to change this week:
    //public int budget_change_of_the_week;
    //How the happiness is going to change this week:
    //public int happy_change_of_the_week;
    //How the health is going to change this week:
    //public int health_change_of_the_week;
    //How the energy is going to change this week:
    //public int energy_change_of_the_week;
    //How the social satisfaction is going to change this week:
    //public int socialSatisfaction_change_of_the_week;
    //Informs how many feedback sentences there are
    //public int feedbacks_int;
    //Informs the feedback sentences (used when there are neutral events)
    //public List<string> feedback_list;
    //Informs that the budget impact is ready to be sent
    //public bool impact_of_choices_ready = false;


    //                                        ##############variables for Gamemanager##############
    private int which_mini_game;
    private EventsManager.changes_this_week changes;


    //---------------------------------------------****SINGLETON****------------------------------------------//

    private static RealGameManager _instance;
    public static RealGameManager instance
    {
        get
        {
            if (_instance == null){
                _instance = GameObject.FindObjectOfType<RealGameManager>();
                if (_instance == null)
                {
                    GameObject g = new GameObject("RealGameManangerssss");
                    _instance = g.AddComponent<RealGameManager>();
                }
            }
            return _instance;
        }
    }

    //dans un autre scritp on écrit RealGameManager.instance
    
    //----------------------------------------------------------------------------------------------------------//




    /// ###############################################################################################
    /// ##########################          Scene change            ###################################
    /// ###############################################################################################



    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    
    //Start the game
    void Start()
    {
        Week = 1;
        Month = 1;
        Happiness = 40;
        Health = 40;
        Social_health = 40;
        Energy = 40;
        Budget_real_time = 0;
        choices_player = new List<int>();

        /*
        events_selection_ready = false;
        impact_of_choices_ready = false;
        budget_change_of_the_week = 0;
        happy_change_of_the_week = 0;
        health_change_of_the_week = 0;
        energy_change_of_the_week = 0;
        socialSatisfaction_change_of_the_week = 0;
        Events_Of_The_Week = new List<EventsSent>();
        
        */

        //SceneManager.LoadScene("Manager_Scene");

    }

    //Change to mini_game
    string menuSceneNameLaunched = "";
    public void Change_to_mini_game()
    {

        //FIX THIS LATER!!!!!!!!!!

        int ran_num = Random.Range(0, 3);

        //int ran_num = 0;
        which_mini_game = ran_num;

        if (ran_num == 0)
        {
            menuSceneNameLaunched = "MenuClick";
                
        }
        if (ran_num == 1)
        {
            menuSceneNameLaunched = "MenuMemory";
            
        }
        if (ran_num == 2)
        {
            menuSceneNameLaunched = "MenuDrop";
            
            
        }

        // Desactiver le canves main
        UIManagerScript.instance.GotoGameScene();
        SceneManager.LoadScene(menuSceneNameLaunched,  LoadSceneMode.Additive);
    }

    // Change back to UI, show imformation, make option.....
    //Have no idea about how many scenes UI will set for this part, if more than one, then I will add more funtion here.
    string gameSceneNameLaunched = "";
    public void Change_to_UI()    
    {

        //Debug.Log("health value"+ Health);
        //####ask for event####
        if (which_mini_game == 0)
        {
            mini_game_score = Manager.ScoreClick;
            gameSceneNameLaunched = "GameClick";
        }
        if (which_mini_game == 1)
        {
            mini_game_score = MemoryManager.MemoryScore;
            gameSceneNameLaunched =  "GameMemory";
        }
        if (which_mini_game == 2)
        {
            mini_game_score = ScoreController.ScoreDrop;
            gameSceneNameLaunched = "GameDrop";
        }
        if (ControlDifficulty.difficulty == 1)
        {
            mini_game_effort = "easy";
        }
        if (ControlDifficulty.difficulty == 2)
        {
            mini_game_effort = "medium";
        }
        if (ControlDifficulty.difficulty == 3)
        {
            mini_game_effort = "hard";
        }
        mini_game_info_ready = true;
        //mini_game_score_int = (int)mini_game_score;


        //##### get events_ 
        //Debug.Log("score:" + mini_game_score);
        events_= EventsManager.instance.EventRequirement_(mini_game_info_ready, mini_game_effort, mini_game_score);
        
        mini_game_info_ready = false;

        UIManagerScript.instance.new_month(Month, Happiness, Health, Energy, Social_health, Budget_real_time);

        // Check si es lou Bueno Endroite
        SceneManager.UnloadSceneAsync(gameSceneNameLaunched);
        UIManagerScript.instance.GotoNewMonthCanvas();
    }

    
    private int indexEvent_ = 0;
    private bool controlFlow = false;
    public void first_week_of_the_month(){
        int count_ = events_.Count;
        if (indexEvent_ < count_){
            UIManagerScript.instance.EventPresentationScene(events_[indexEvent_].event_type, events_[indexEvent_].event_text, events_[indexEvent_].answer_1, events_[indexEvent_].answer_2);
            UIManagerScript.instance.GotoEventsCanvas();
            indexEvent_ = indexEvent_ + 1;
            controlFlow = true;
        }
        else{
            changes = EventsManager.instance.BudgetScoreChangesRequirement_(choices_player);
            calcules(changes);
            UIManagerScript.instance.score_presentation(Month, Week, Happiness, Health, Energy, Social_health, Budget_real_time, changes.feedback_count, changes.feedback);
            UIManagerScript.instance.GotoResultsCanvas();
        }
        
    }

    
    public void players_choosing_answers(int choice_int){
        if(controlFlow == true){
            choices_player.Add(choice_int);
            controlFlow = false;
        }
        
    }

    private void calcules(EventsManager.changes_this_week changes){
        Budget_real_time = Budget_real_time + changes.budget_week_i;
        Happiness = Happiness + changes.happy_week_i;
        Health = Health + changes.health_week_i;
        Energy = Energy + changes.energy_week_i;
        Social_health = Social_health + changes.socialSatisfaction_week_i;
        if (Happiness > 40)
        {
            Happiness = 40;
        }
        if (Health > 40)
        {
            Health = 40;
        }
        if (Energy > 40)
        {
            Energy = 40;
        }
        if (Social_health > 40)
        {
            Social_health = 40;
        }
    }

    public void next_week(){
        choices_player = new List<int>();
        indexEvent_ = 0;
        controlFlow = false;
        Game_is_end = false;


         /*to test :
        Energy = Energy - 50;
        Debug.Log("enter next week");
        Debug.Log("Ha" + Happiness + "he" + Health + "en" + Energy + "ss" + Social_health);
        */

        if ((Budget_real_time<0)||(Happiness<0)||(Health<0)||(Energy<0)||(Social_health<0))
        {
            Debug.Log("EndGame should work");
            UIManagerScript.instance.GotoEndOfGame();
            Game_is_end = true;
        }

        
        if (Week == 4){
            if (Month == 6)
            {
                Debug.Log("EndGame should work");
                UIManagerScript.instance.GotoEndOfGame();
                Game_is_end = true;
            }
            else{
                Month = Month + 1;
                Week = 0;
            }
        }
        if(Game_is_end == false){
            Week = Week + 1;
            if(Week == 1){
                Change_to_mini_game();
            }
            else{
                events_= EventsManager.instance.EventRequirement_(mini_game_info_ready, mini_game_effort, mini_game_score_int);
                first_week_of_the_month();
            }
        }
    }

    public void ReStartGame(){
        Week = 1;
        Month = 1;
        Happiness = 40;
        Health = 40;
        Social_health = 40;
        Energy = 40;
        Budget_real_time = 0;
        choices_player = new List<int>();
        indexEvent_ = 0;
        controlFlow = false;
        Game_is_end = false;
        EventsManager.instance.ReStartGame_();
        UIManagerScript.instance.GotoInitialCanvas();
    }

    //class to prepare the information sent to game manager//events sent for the player
    public class EventsSent
    {
        public string event_text;
        public string answer_1;
        public string answer_2;
        //fixed event = 0; variable event = 1;
        public int event_type;
    }

    public class changes_this_week{
        public int budget_week_i;  
        public int happy_week_i;
        public int health_week_i;
        public int energy_week_i;
        public int socialSatisfaction_week_i;
        public int feedback_count;
        public List<string> feedback;
    } 

/*
    public void End_of_the_week()
    {

        //SceneManager.LoadScene("End_week"); // Maybe we don't have this scene, no problem, delete this line.
        // just don't forget to call this function at the end of a week


        //#####demand change of the week:
        changes = EventsManager.instance.BudgetScoreChangesRequirement_(_player);
        budget_change_of_the_week = changes.budget_week_i;
        happy_change_of_the_week = changes.happy_week_i;
        health_change_of_the_week = changes.health_week_i;
        energy_change_of_the_week = changes.energy_week_i;
        socialSatisfaction_change_of_the_week = changes.socialSatisfaction_week_i;

        //##change the value
        Health = Health + happy_change_of_the_week;
        Energy = Energy + energy_change_of_the_week;
        happiness = happiness + happy_change_of_the_week;
        Social_health = Social_health + socialSatisfaction_change_of_the_week;
        Budget_real_time = Budget_real_time + budget_change_of_the_week;
        
        
        Week = Week + 1;
        if (Week == 5)
        {
            Month = Month + 1;
            Week = 1;
            if ((Month == 12) || (Budget_real_time<0))           
            {
                Game_is_end = true;
                SceneManager.LoadScene("UI"); //End of the game

            }
        }

        //SceneManager.LoadScene("New_week ");  //// Maybe we don't have this scene, no problem, delete this line;
                                              //// just don't forget to call a function to change a scene at the begining of the week;
    }



    /// ###############################################################################################
    /// ##########################          Class definition         ###################################
    /// ###############################################################################################

    //class for the values that are randomly chosen in the events description
    private class VariableInfo
    {
        public int int_value;
        public string st_value;

        public VariableInfo(int i, string s)
        {
            int_value = i;
            st_value = s;

        }
    }

    //class to define the fixed events
    private class FixedEventsInfo
    {
        public string fixed_event;
        public int immediate_consequence;
    }

    //class to define the positive and negative events
    private class VariableEvents
    {
        public string variable_event;
        public string answer_accept;
        public string answer_reject;
        public bool can_happen_multiple_times;
        public int multual_exclusive;
        public int late_consequence;
        public int immediate_consequence;
    }

    //class to define neutral events
    private class NeutralEventsInfo
    {
        public string neutral_event;
        public string answer_accept;
        public string answer_reject;
        public string result_positive;
        public string result_negative;
        public int immediate_consequence;
        public int late_consequence_positive;
    }

    //class to define dependent events
    private class DependentEventsInfo
    {
        public string dependent_event;
        public string answer_accept;
        public string answer_reject;
        public bool can_happen_multiple_times;
        public bool there_is_dependency;
        public int immediate_consequence;
    }

    

    //class to prepare the information sent to game manager//impact of player's choices
    private class return_parameters
    {
        public int budget_week_i;
        public int budget_week_i_plus_1;
        public int feedback_count;
        public List<string> feedback;
    }
*/

}


