#README

In this folder you will find the data used for the development of our game called "Paul Financier".

In Assets, you can see the work done by each one of us. 

In the "Lucas" folder, you can find all the information regarding the mini-games in our game, as well as the assets used for their development.

In the "Leticia" folder, you can find the codes referring to the management of all events, scenes, and their impact on the action bars.

In the "Ricardo" folder, you can find the UI codes.

In the "Yuanxian" folder, you can find the codes for the GameManager, which we call "RealGameManager", which organizes all our features present in the game.

Finally, you should start the game using the scene "Manager_Scene", located in the Assets folder of our game. In this scene, you can find all the UI elements, which are activated by our GameManager. Also, all of these elements are linked to the mini-game scenes in the "Lucas" folder.

The whole game is designed to be played on a Xiaomi Redmi 8*, you will probably find some misplaced blocks if you try to play on a computer. For that, choose the correct dimension (720 x 1520) for the screen game.

In addition, you can find in this same path a pdf file called "Presentation", which explains the mechanics of our game, as well as listing the events and their weights for the player.

*You can find the build in .apk in the "build" folder.
