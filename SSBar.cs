using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SSBar : MonoBehaviour
{
    public Slider SocialSBar;

    private void Start()
    {
        SocialSBar = GetComponent<Slider>();
        SocialSBar.maxValue = 100;
    }

    public void SetSS(int hpp)
    {
        SocialSBar.value = hpp;
    }
}
