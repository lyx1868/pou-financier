using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Testing : MonoBehaviour
{

    public bool events_requirement = false;
    public bool events_choices_ready = false;
    private int s;
    private bool t;
    private List<EventsManager.EventsSent> events_;
    private int week;
    private int month;
    private int k;
    private List<int> _player;
    private List<int> BinaryVariables = new List<int> {0,1};
    private EventsManager.changes_this_week changes;
    private float time_1;
    private float time_2;
    private int counting;
    //private int counting_interval;
    private bool control;

    // Start is called before the first frame update
    void Start()
    {
        s = 0;
        week = 1;
        month = 1;
        k = 0;
        counting = 0;
        //counting_interval = 0;
        control = false;
        time_1 = 0;
        time_2 = 0;
    }


    // Update is called once per frame
    void Update()
    {
        if(events_requirement == true){
            if(s == 0){
                t = true;
                s = 1;
            }
            events_ = EventsManager.instance.EventRequirement_(t,"easy",2);
            for (int i = 0; i < events_.Count; i++)
            {
                Debug.Log("event: " + events_[i].event_text + " week: " + week + " month: " + month);
                if (events_[i].event_type != 0){
                    k = k + 1;
                    Debug.Log("answer1:" + events_[i].answer_1 + " week: " + week + " month: " + month);
                    Debug.Log("answer2:" + events_[i].answer_2 + " week: " + week + " month: " + month);
                }
            }
            events_requirement = false;
            t = false; 
        }

        if(events_choices_ready == true){

            _player = new List<int>();
            for (int i = 0; i < k; i++){
                _player.Add(GetRandomValue(BinaryVariables));
                Debug.Log("player's choice: " + _player[i] + " week: " + week + " month: " + month);
            }
            changes = EventsManager.instance.BudgetScoreChangesRequirement_(_player);
            
            Debug.Log("budget this week: " + changes.budget_week_i + " week: " + week + " month: " + month);     
            Debug.Log("happy this week: " + changes.happy_week_i + " week: " + week + " month: " + month);
            Debug.Log("health this week: " + changes.health_week_i + " week: " + week + " month: " + month);
            Debug.Log("energy this week: " + changes.energy_week_i + " week: " + week + " month: " + month);
            Debug.Log("satisfaction this week: " + changes.socialSatisfaction_week_i + " week: " + week + " month: " + month);
            for (int i = 0; i < changes.feedback_count; i++){
                Debug.Log("feedback: " + changes.feedback[i] + " week: " + week + " month: " + month);
            }
            

            events_choices_ready = false;
            if (week == 4){
                if (month == 6){
                    month = 0;
                }
                month = month + 1;
                week = 0;
                s = 0;
            }
            week = week + 1;
            k = 0;
        }

        if(counting <= 26){
            if(time_1 >= 2){
                if(control == false){
                    events_requirement = true;
                    control = true;
                }
                else{
                    if(time_2 >= 2){
                        events_choices_ready = true;
                        control = false;
                        time_1 = 0;
                        time_2 = 0;
                        counting = counting + 1;
                    }
                    else{
                        time_2 = time_2 + Time.deltaTime;
                    }
                } 
            }
            else{
                time_1 = time_1 + Time.deltaTime;
            }
        }
        
        
    }

    private int GetRandomValue(List<int> list_of_values){
        int index = Random.Range(0, (list_of_values.Count));
        return list_of_values[index];
    }

    /*
    public class EventsSent{
        public string event_text;
        public string answer_1;
        public string answer_2;
        //fixed event = 0; variable event = 1;
        public int event_type;
    }

    public class changes_this_week{
        public int budget_week_i;  
        public int happy_week_i;
        public int health_week_i;
        public int energy_week_i;
        public int socialSatisfaction_week_i;
        public int feedback_count;
        public List<string> feedback;
    } 
    */
}

