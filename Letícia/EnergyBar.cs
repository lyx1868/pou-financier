using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyBar : MonoBehaviour
{
    public Slider energyBar;

    private void Start()
    {
        energyBar = GetComponent<Slider>();
        energyBar.maxValue = 100;
    }

    public void SetEnergy(int hpp)
    {
        energyBar.value = hpp;
    }
}
