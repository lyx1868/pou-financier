using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerScript : MonoBehaviour
{
    public Text month_LevelSelectPanel;
    public Text Budget_LevelSelectPanel;
    private string budget_LSP_st;
    private string month_LSP;
    
    /*
    //public HappinessBar new_month_playerHappiness;
    public HealthBar new_month_playerHealth;
    public EnergyBar new_month_playerEnergy;
    public SSBar new_month_playerSS;
    */

    public GameObject new_month_playerHappiness;
    public GameObject new_month_playerHealth;
    public GameObject new_month_playerEnergy;
    public GameObject new_month_playerSS;



    public Text month_ScorePanel;
    public Text week_ScorePanel;
    public Text Budget_ScorePanel;
    private string budget_ScorePanel_st;
    private string month_ScorePanel_st;

    /*
    public HappinessBar scorePanel_playerHappiness;
    public HealthBar scorePanel_playerHealth;
    public EnergyBar scorePanel_playerEnergy;
    public SSBar scorePanel_playerSS;
    */

    public GameObject scorePanel_playerHappiness;
    public GameObject scorePanel_playerHealth;
    public GameObject scorePanel_playerEnergy;
    public GameObject scorePanel_playerSS;

    public GameObject imageFeedback_scorePanel;
    public GameObject feedback1_scorePanel;
    public GameObject feedback2_scorePanel;
    public GameObject feedback3_scorePanel;
    public Text text_feed1_scorePanel;
    public Text text_feed2_scorePanel;
    public Text text_feed3_scorePanel;


    public Text Text_UIEvents;
    public GameObject imageA1_UIEvents;
    public GameObject imageA2_UIEvents;
    public GameObject answer1_UIEvents;
    public Text TextA1_UIEvents;
    public GameObject answer2_UIEvents;
    public Text TextA2_UIEvents;
    public GameObject A1button_UIEvents;
    public GameObject A2button_UIEvents;
    public GameObject NextButton;


    public GameObject canvasMainScene;
    public GameObject mainPanel;
    public GameObject newMonthPanel;
    public GameObject eventsPanel;
    public GameObject resultsPanel;
    public GameObject endGamePanel;

    //---------------------------------------------****SINGLETON****------------------------------------------//

    private static UIManagerScript _instance;
    public static UIManagerScript instance
    {
        get
        {
            if (_instance == null){
                _instance = GameObject.FindObjectOfType<UIManagerScript>();
            }
            return _instance;
        }
    }

    //dans un autre scritp on écrit UIManagerScript.instance
    
    //----------------------------------------------------------------------------------------------------------//

    void Start(){
        canvasMainScene.SetActive(true);
        mainPanel.SetActive(true);
        newMonthPanel.SetActive(false);
        eventsPanel.SetActive(false);
        resultsPanel.SetActive(false);
        endGamePanel.SetActive(false);

        
        TextA1_UIEvents = answer1_UIEvents.GetComponent<Text>();
        TextA2_UIEvents = answer2_UIEvents.GetComponent<Text>();

        text_feed1_scorePanel = feedback1_scorePanel.GetComponent<Text>();
        text_feed2_scorePanel = feedback2_scorePanel.GetComponent<Text>();
        text_feed3_scorePanel = feedback3_scorePanel.GetComponent<Text>();
    }

    public void StartGame_Requirement(){
        RealGameManager.instance.Change_to_mini_game();
    }

    public void GotoMainScene()
    {
        canvasMainScene.SetActive(true);
    }

     public void GotoGameScene()
    {
        canvasMainScene.SetActive(false);
    }

    public void GotoNewMonthCanvas(){
        mainPanel.SetActive(false);
        eventsPanel.SetActive(false);
        resultsPanel.SetActive(false);
        endGamePanel.SetActive(false);
        newMonthPanel.SetActive(true);
        canvasMainScene.SetActive(true);
    }

    public void GotoEventsCanvas(){
        mainPanel.SetActive(false);
        resultsPanel.SetActive(false);
        endGamePanel.SetActive(false);
        newMonthPanel.SetActive(false);
        eventsPanel.SetActive(true);
        canvasMainScene.SetActive(true);
    }

    public void GotoResultsCanvas(){
        mainPanel.SetActive(false);
        endGamePanel.SetActive(false);
        newMonthPanel.SetActive(false);
        eventsPanel.SetActive(false);
        resultsPanel.SetActive(true);
        canvasMainScene.SetActive(true);
    }

    public void GotoInitialCanvas(){
        endGamePanel.SetActive(false);
        newMonthPanel.SetActive(false);
        eventsPanel.SetActive(false);
        resultsPanel.SetActive(false);
        mainPanel.SetActive(true);
        canvasMainScene.SetActive(true);
    }

    public void GotoEndOfGame(){
        //Debug.Log("Yes, We shold see the scene with end");
        newMonthPanel.SetActive(false);
        eventsPanel.SetActive(false);
        resultsPanel.SetActive(false);
        mainPanel.SetActive(false);
        endGamePanel.SetActive(true);
        canvasMainScene.SetActive(true);
    }


    public void new_month(int month, int happy, int health, int energy, int SS, float budget){

        if(month == 1){
            month_LSP = "Jan";
        }
        else{
            if(month == 2){
                month_LSP = "Feb";
            }
            else{
                if(month == 3){
                    month_LSP = "Mar";
                }
                else{
                    if(month == 4){
                        month_LSP = "Apr";
                    }
                    else{
                        if(month == 5){
                            month_LSP = "May";
                        }
                        else{
                            month_LSP = "Jun";
                        }
                    }
                }
            }
        }
        month_LevelSelectPanel.text = month_LSP;
        new_month_playerHappiness.GetComponent<SomethingBar>().SetBar(happy);
        new_month_playerHealth.GetComponent<SomethingBar>().SetBar(health);
        new_month_playerEnergy.GetComponent<SomethingBar>().SetBar(energy);
        new_month_playerSS.GetComponent<SomethingBar>().SetBar(SS);
        /*
        //new_month_playerHappiness.SetHappiness(happy);
        //Debug.Log("happy show"+ new_month_playerHappiness.happinessBar.value);
        new_month_playerHealth.SetHealth(health);
        Debug.Log("health show"+ new_month_playerHealth.healthBar.value);
        //Debug.Log("the happy in UI is "+ happy);
        new_month_playerEnergy.SetEnergy(energy);
        Debug.Log("energy show"+ new_month_playerEnergy.energyBar.value);
        new_month_playerSS.SetSS(SS);
        Debug.Log("SS show"+ new_month_playerSS.SocialSBar.value);
        */
        budget_LSP_st = budget.ToString();
        Budget_LevelSelectPanel.text = budget_LSP_st;
    }

    public void score_presentation(int month, int week, int happy, int health, int energy, int SS, float budget, int feedb_count, List<string> feedb){

        if(month == 1){
            month_ScorePanel_st = "Jan";
        }
        else{
            if(month == 2){
                month_ScorePanel_st = "Feb";
            }
            else{
                if(month == 3){
                    month_ScorePanel_st = "Mar";
                }
                else{
                    if(month == 4){
                        month_ScorePanel_st= "Apr";
                    }
                    else{
                        if(month == 5){
                            month_ScorePanel_st = "May";
                        }
                        else{
                            month_ScorePanel_st = "Jun";
                        }
                    }
                }
            }
        }
        month_ScorePanel.text = month_ScorePanel_st;
        /*
        scorePanel_playerHappiness.SetHappiness(happy);
        Debug.Log("happy show"+ scorePanel_playerHappiness.happinessBar.value);
        //Debug.Log("the happy in UI is "+ happy);
        scorePanel_playerHealth.SetHealth(health);
        Debug.Log("health show"+ scorePanel_playerHealth.healthBar.value);
        scorePanel_playerEnergy.SetEnergy(energy);
        Debug.Log("energy show"+ scorePanel_playerEnergy.energyBar.value);
        scorePanel_playerSS.SetSS(SS);
        Debug.Log("SS show"+ scorePanel_playerSS.SocialSBar.value);
        */
        scorePanel_playerHappiness.GetComponent<SomethingBar>().SetBar(happy);
        scorePanel_playerHealth.GetComponent<SomethingBar>().SetBar(health);
        scorePanel_playerEnergy.GetComponent<SomethingBar>().SetBar(energy);
        scorePanel_playerSS.GetComponent<SomethingBar>().SetBar(SS);
        budget_ScorePanel_st = budget.ToString();
        Budget_ScorePanel.text = budget_ScorePanel_st;
        week_ScorePanel.text = week.ToString();
        if(feedb_count == 0){
            imageFeedback_scorePanel.SetActive(false);
            feedback1_scorePanel.SetActive(false);
            feedback2_scorePanel.SetActive(false);
            feedback3_scorePanel.SetActive(false);
        }
        else{
            imageFeedback_scorePanel.SetActive(true);
            if(feedb_count == 1){
                feedback1_scorePanel.SetActive(true);
                text_feed1_scorePanel.text = feedb[0];
                feedback2_scorePanel.SetActive(false);
                feedback3_scorePanel.SetActive(false);
            }
            else{
                if(feedb_count == 2){
                    feedback1_scorePanel.SetActive(true);
                    text_feed1_scorePanel.text = feedb[0];
                    feedback2_scorePanel.SetActive(true);
                    text_feed2_scorePanel.text = feedb[1];
                    feedback3_scorePanel.SetActive(false);
                }
                else{
                    if(feedb_count == 3){
                        feedback1_scorePanel.SetActive(true);
                        text_feed1_scorePanel.text = feedb[0];
                        feedback2_scorePanel.SetActive(true);
                        text_feed2_scorePanel.text = feedb[1];
                        feedback3_scorePanel.SetActive(true);
                        text_feed3_scorePanel.text = feedb[2];
                    }
                }
            }
        }
    }


    public void EventPresentationScene(int EventType, string EventText, string Answer1, string Answer2){
        if(EventType == 0){
            imageA1_UIEvents.SetActive(false);
            imageA2_UIEvents.SetActive(false);
            answer1_UIEvents.SetActive(false);
            answer2_UIEvents.SetActive(false);
            A1button_UIEvents.SetActive(false);
            A2button_UIEvents.SetActive(false);
            NextButton.SetActive(true);
            Text_UIEvents.text = EventText;
        }
        else{
            imageA1_UIEvents.SetActive(true);
            imageA2_UIEvents.SetActive(true);
            answer1_UIEvents.SetActive(true);
            answer2_UIEvents.SetActive(true);
            A1button_UIEvents.SetActive(true);
            A2button_UIEvents.SetActive(true);
            Text_UIEvents.text = EventText;
            TextA1_UIEvents.text = Answer1;
            TextA2_UIEvents.text = Answer2;
            NextButton.SetActive(false);
        } 
    }

    public void Answer1Choice(){
        RealGameManager.instance.players_choosing_answers(0);
        NextButton.SetActive(true);
    }

    public void Answer2Choice(){
        RealGameManager.instance.players_choosing_answers(1);
        NextButton.SetActive(true);
    }
}
