using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SomethingBar : MonoBehaviour
{
    public Slider somethingBar;

    private void Start()
    {
        somethingBar = GetComponent<Slider>();
        somethingBar.maxValue = 40;
        somethingBar.value = 40;

    }

    public void SetBar(int value_)
    {
        somethingBar.value = value_;
    }
}
