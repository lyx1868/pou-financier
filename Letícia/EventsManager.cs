using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventsManager : MonoBehaviour
{
    /*
    public delegate void OnAccountChangeDelegate (int previousAccount, int newAccount);
    public event OnAccountChangeDelegate OnAccountChange;

    public delegate void HappinessChanges(int happy_this_week);
    public event HappinessChanges OnHappinessChange;
    */

    //---------------------------------------------****VARIABLES****------------------------------------------//

    //Variables for Fixed Events:
    //Keep the fixed events
    private List<FixedEventsInfo> FixedEvents;
    //Possible values for some events
    private List<int> ConnectionBills = new List<int> {20, 25, 28, 30, 35, 37, 40};
    private List<int> UtilitiesBills = new List<int> {115, 125, 127, 130, 135};
    private List<int> GroceriesBills = new List<int> {110, 118, 120, 125, 129, 135};
    //Keep the values that were randomly chosen
    private List<VariableInfo> FE_randomInfo;

    //Variables for Variable Positive Events:
    //Keep the positive events
    private List<VariableEvents> PositiveEvents;
    //Possible values for some events
    private List<int> EurosFound_Borrowed = new List<int> {5, 10, 15, 25, 50};
    private List<int> HeritageObtained = new List<int> {100, 150, 250};
    private List<int> InsuranceObtained = new List<int> {45, 50, 60};
    private List<int> PictOfDog = new List<int> {7, 15, 18};
    private List<VariableInfo> PE_randomInfo;

    //Variables for Variable Negative Events:
    //Keep the negative events
    private List<VariableEvents> NegativeEvents;
    //Possible values for some events
    private List<int> CatchUpFriend = new List<int> {10, 20, 30};
    private List<int> ShowTickets = new List<int> {60, 76, 89};
    private List<int> BeachPromotion = new List<int> {80, 100, 120};
    private List<int> CoffeeMoney = new List<int> {5, 8, 10, 12, 20};
    //Keep the values that were randomly chosen
    private List<VariableInfo> NE_randomInfo;

    //Variables for Neutral Events:
    //Keep the neutral events
    private List<NeutralEventsInfo> NeutralEvents;
    //Possible values for some events
    private List<int> LotteryMoney = new List<int> {50, 100, 150, 200};
    //Keep the values that were randomly chosen
    private List<VariableInfo> NEut_randomInfo;

    //Variables for Dependent Events:
    //Keep the dependent events
    private List<DependentEventsInfo> DependentEvents;
    private bool wife_pregnant = false;


    /*
    //Variables to exchange information with game manager:
    //THIS VARIABLES ARE GIVEN BY THE GAME MANAGER:
    //asks for new events
    public bool events_requirement = false;
    //informs that the player has finished the mini-game
    public bool mini_game_info_ready = false;
    //player's choice on the effort 
    public string mini_game_effort;
    //player's score on the mini game
    public int mini_game_score;
    //informs that the player has finished to make its choices
    public bool events_choices_ready = false;
    //player's choices for each question: 0 -> answer 1 was chosen, 1 -> answer 2 was chosen
    //PAY ATTENTION TO THIS VARIABLE !!!!!!!!!!!!!!!!!
    public List<int> choices_player = new List<int>();
    //THIS VARIABLES ARE GIVEN TO THE GAME MANAGER:
    //Informs that the events are ready to be sent
    public bool events_selection_ready = false;  
    //Events selected for the week
    public List<EventsSent> Events_Of_The_Week;
    //How the budget is going to change this week:
    public int budget_change_of_the_week;
    //How the happiness is going to change this week:
    public int happy_change_of_the_week;
    //How the health is going to change this week:
    public int health_change_of_the_week;
    //How the energy is going to change this week:
    public int energy_change_of_the_week;
    //How the social satisfaction is going to change this week:
    public int socialSatisfaction_change_of_the_week;
    //Informs how many feedback sentences there are
    public int feedbacks_int;
    //Informs the feedback sentences (used when there are neutral events)
    public List<string> feedback_list;
    //Informs that the budget impact is ready to be sent
    public bool impact_of_choices_ready = false;
    */

    //Transformation of the score into a bonus
    private float bonus_payment = 100;
    private string st_bonus_payment;
    private int effort;

    //Keep track of the week and month that we are
    private int week;
    private int month;
    //How many variable questions are going to appear this week? 
    private List<int> NumberOfVariableQuestions = new List<int> {1,2,3};
    //Indexes of positive and negative events to control the selection of events;
    private List<int> PositiveEventsIndexes = new List<int> {0,1,2,3,4,5};
    private List<int> NegativeEventsIndexes = new List<int> {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
    private List<int> NeutralEventsIndexes = new List<int> {0,1,2};
    private List<int> DependentEventsIndexes = new List<int> {0,1,2,3};
    //To differentiate between positive (0), negative (1), neutral (2) and dependent (3) events
    private List<int> TypeOfVariableQuestion = new List<int> {0,1,2,3};
    //For testing and for handling the randomness of neutral events when they are chosen
    private List<int> BinaryVariables = new List<int> {0,1};
    //Keep the index and type (0 -> positive, 1 -> negative) of the events randomly chosen for the week
    private List<int> QuestionsChosenType;
    private List<int> QuestionsChosenIndexes;
    private bool control_1;
    private bool control_2;
    //how the decisions made this week change the budget in the week and in the next
    private return_parameters impacts_of_the_week;
    private float budget_update_following_week = 0;
    

    //----------------------------------------------------------------------------------------------------------//

    //---------------------------------------------****SINGLETON****------------------------------------------//

    private static EventsManager _instance;
    public static EventsManager instance
    {
        get
        {
            if (_instance == null){
                _instance = GameObject.FindObjectOfType<EventsManager>();
            }
            return _instance;
        }
    }

    //dans un autre scritp on écrit EventsManager.instance
    
    //----------------------------------------------------------------------------------------------------------//
    

    //---------------------------------------------****INITIAL CONFIGURATIONS****------------------------------------------//

    // Start is called before the first frame update

    void Start()
    {
        week = 1;
        month = 1;
        //events_selection_ready = false;
        //impact_of_choices_ready = false;
        budget_update_following_week = 0;
        //budget_change_of_the_week = 0;
        //happy_change_of_the_week = 0;
        //health_change_of_the_week = 0;
        //energy_change_of_the_week = 0;
        effort = 0;
        //socialSatisfaction_change_of_the_week = 0;
        //Events_Of_The_Week = new List<EventsSent>();
        
        QuestionsChosenType = new List<int>();
        QuestionsChosenIndexes = new List<int>();
        wife_pregnant = false;
        control_1 = false;
        control_2 = false;

        //Preparing list of Fixed Events
        st_bonus_payment = bonus_payment.ToString();
        FE_randomInfo = GetListOfVariableValuesForFixedEvents(ConnectionBills, UtilitiesBills, GroceriesBills);
        FixedEvents = new List<FixedEventsInfo>(){
            new FixedEventsInfo{fixed_event = "Your paycheck is here! +1500 euros in your account. Yey!", immediate_consequence = 1500},
            new FixedEventsInfo{fixed_event = "Here's your bonus for all the hard work! +" + st_bonus_payment + " euros for you.", immediate_consequence = bonus_payment},
            new FixedEventsInfo{fixed_event = "Electricity, cable TV, Internet... Gotta pay the bills to stay connected. Must pay " + FE_randomInfo[0].st_value + " euros.", immediate_consequence = FE_randomInfo[0].int_value},
            new FixedEventsInfo{fixed_event = "It's that time of the month again... Must pay rent. Pay 700 euros.", immediate_consequence = 700},
            new FixedEventsInfo{fixed_event = "Water and gaz bills arrived. Looks like natural ressources ain't free afterall. What a bummer! Must pay " + FE_randomInfo[1].st_value + " euros.", immediate_consequence = FE_randomInfo[1].int_value},
            new FixedEventsInfo{fixed_event = "Went to the supermarket for groceries and essentials buys. A guy gotta eat... Must pay now " + FE_randomInfo[2].st_value + " euros.", immediate_consequence = FE_randomInfo[2].int_value},
        };
        
        //Preparing list of Positive Events
        PE_randomInfo = GetListOfVariableValuesForPositiveEvents(EurosFound_Borrowed, HeritageObtained, InsuranceObtained, PictOfDog);
        PositiveEvents = new List<VariableEvents>(){
            new VariableEvents{variable_event = "Wow. Look, there's " + PE_randomInfo[0].st_value + " euros in the street! Do you take it? You kinda shouldn't...", answer_accept = "Take it, no one is looking.", answer_reject = "Leave it, maybe someone will come for it later.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = PE_randomInfo[0].int_value, happy_consequence_yes = 5, happy_consequence_no = -4, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = -3, satisfaction_consequence_no = 6},
            new VariableEvents{variable_event = "You closed an important project at work. The boss looks happy. Maybe you should ask him for that so promised 80 euros bonus. But if you do he might get a little uncomfortable and uninvite you to dinner tonight...", answer_accept = "Ask him.", answer_reject = "Maybe next time...A good relationship with the boss is more important.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 80, happy_consequence_yes = 5, happy_consequence_no = -4, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = -7, satisfaction_consequence_no = 6},
            new VariableEvents{variable_event = "Oh no, your not so distant aunt Beth has passed away. But, since you were the sweetest nephew, aunty left you " + PE_randomInfo[1].st_value + " euros as heritage.", answer_accept = "Save the heritage money.", answer_reject = "Spend it in a weekend in Italy.", can_happen_multiple_times = false, multual_exclusive = 12, late_consequence = 0, immediate_consequence = PE_randomInfo[1].int_value, happy_consequence_yes = 5, happy_consequence_no = 8, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = -6, satisfaction_consequence_yes = -3, satisfaction_consequence_no = 4},
            new VariableEvents{variable_event = "A friend owes you " + PE_randomInfo[2].st_value + " euros. Ask him for the money? He has been so upset lately...", answer_accept = "Ask him for the money!", answer_reject = "Forget it, his friendship is more worthy.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = PE_randomInfo[2].int_value, happy_consequence_yes = 3, happy_consequence_no = -2, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = -5, satisfaction_consequence_no = 4},
            new VariableEvents{variable_event = "Oh, no. You were ran over by an ice-cream truck. At least insurance will pay you " + PE_randomInfo[3].st_value + " euros and you didn't break any bones.", answer_accept = "Save the insurance money.", answer_reject = "Spend it in a cool dinner.", can_happen_multiple_times = false, multual_exclusive = 16, late_consequence = 0, immediate_consequence = PE_randomInfo[3].int_value, happy_consequence_yes = 5, happy_consequence_no = 2, health_consequence_yes = -6, health_consequence_no = -6, energy_consequence_yes = -2, energy_consequence_no = -2, satisfaction_consequence_yes = -1, satisfaction_consequence_no = 2},
            new VariableEvents{variable_event = "Sold a cool picture of your dog to the local newspaper. Here's " + PE_randomInfo[4].st_value + " euros for your contribution.", answer_accept = "Save the money.", answer_reject = "Cool. Going to buy a treat for my beautiful dog.", can_happen_multiple_times = false, multual_exclusive = 0, late_consequence = 0, immediate_consequence = PE_randomInfo[4].int_value, happy_consequence_yes = 3, happy_consequence_no = -3, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = -2, energy_consequence_no = -2, satisfaction_consequence_yes = 1, satisfaction_consequence_no = 3}
        };
        
        //Preparing list of Negative Events
        NE_randomInfo = GetListOfVariableValuesForNegativeEvents(EurosFound_Borrowed, CatchUpFriend, ShowTickets, BeachPromotion, CoffeeMoney);
        NegativeEvents = new List<VariableEvents>(){
            new VariableEvents{variable_event = "Oh no, your not so distant aunt Beth has passed away. But you were her only nephew, gotta pay 90 euros to set up her funeral.", answer_accept = "Pay it now.", answer_reject = "Pay it +10 next week.", can_happen_multiple_times = false, multual_exclusive = 8, late_consequence = 100, immediate_consequence = 90, happy_consequence_yes = -8, happy_consequence_no = -10, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = -4, energy_consequence_no = -4, satisfaction_consequence_yes = -1, satisfaction_consequence_no = -2},
            new VariableEvents{variable_event = "You owe " + NE_randomInfo[0].st_value +  " euros to a friend. Pay him?", answer_accept = "Pay him!", answer_reject = "Maybe if I wait a little longer, he will forget.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = NE_randomInfo[0].int_value, happy_consequence_yes = -2, happy_consequence_no = 2, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 4, satisfaction_consequence_no = -4},
            new VariableEvents{variable_event = "Bathroon's lamp burned. Are you buying a new one or your next trip to the toilet is going to be in the dark? A new lamp costs 5 euros.", answer_accept = "Buy lamp.", answer_reject = "Peeing in the dark it is.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 5, happy_consequence_yes = -2, happy_consequence_no = -4, health_consequence_yes = 1, health_consequence_no = -1, energy_consequence_yes = -1, energy_consequence_no = 0, satisfaction_consequence_yes = 3, satisfaction_consequence_no = -6},
            new VariableEvents{variable_event = "An accident happened and there's glass on the road. What a bad luck, two flat tires. Pay 30 euros to fix it or take the bicycle to work.", answer_accept = "Fix it.", answer_reject = "A 40 minute bike ride every minute could be what I needed to drop a few pounds...", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 30, happy_consequence_yes = -5, happy_consequence_no = -8, health_consequence_yes = 0, health_consequence_no = 5, energy_consequence_yes = -1, energy_consequence_no = -6, satisfaction_consequence_yes = 0, satisfaction_consequence_no = 0},
            new VariableEvents{variable_event = "Oh, no. You were ran over by an ice-cream truck. Broke your leg and now has to pay 50 euros in hospital bills.", answer_accept = "Pay it now.", answer_reject = "Pay it +12 euros next week.", can_happen_multiple_times = true, multual_exclusive = 10, late_consequence = 62, immediate_consequence = 50, happy_consequence_yes = -5, happy_consequence_no = -8, health_consequence_yes = -6, health_consequence_no = -6, energy_consequence_yes = -2, energy_consequence_no = -2, satisfaction_consequence_yes = -1, satisfaction_consequence_no = -1},
            new VariableEvents{variable_event = "It didn't rain for the past four months and they are charging an extra 10 euros for the excessive use of water resources. Should have checked for leaks... Now you gotta pay.", answer_accept = "Pay now.", answer_reject = "Pay it +10 euros next week and lose water supply 'till next week.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 20, immediate_consequence = 10, happy_consequence_yes = -3, happy_consequence_no = -6, health_consequence_yes = 1, health_consequence_no = -3, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = -2, satisfaction_consequence_no = -6},
            new VariableEvents{variable_event = "Your wife seems so sad lately. Doesn't she deserve a nice perfum? She'll smell so good with this 25 euros perfum...", answer_accept = "Buy it", answer_reject = "She can get happy with something cheaper.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 25, happy_consequence_yes = 3, happy_consequence_no = -2, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = -1, energy_consequence_no = 0, satisfaction_consequence_yes = 5, satisfaction_consequence_no = -1},
            new VariableEvents{variable_event = "Your wedding anniversary is in two days. Shouldn't you take your wife on a fancy date? Dinner for 45 euros sounds good...", answer_accept = "She'll love it!", answer_reject = "We find happiness in simple things. Right?", can_happen_multiple_times = false, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 45, happy_consequence_yes = 5, happy_consequence_no = -4, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 6, satisfaction_consequence_no = -6},
            new VariableEvents{variable_event = "An old friend is in town and she would love to go out to catch up. Just " + NE_randomInfo[1].st_value + " euros to revisit old times...", answer_accept = "Let's do it for the old times", answer_reject = "The past belongs to the past.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = NE_randomInfo[1].int_value, happy_consequence_yes = 3, happy_consequence_no = -1, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = -2, energy_consequence_no = 0, satisfaction_consequence_yes = 5, satisfaction_consequence_no = -5},
            new VariableEvents{variable_event = "Woke up too late for work today. Must work late tonight or lose 10 euros from your paycheck.", answer_accept = "No way I'm staying late tonight.", answer_reject = "Work work work work...", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 10, happy_consequence_yes = -4, happy_consequence_no = -2, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = -7, satisfaction_consequence_yes = -4, satisfaction_consequence_no = -5},
            new VariableEvents{variable_event = "It has been cold lately. Pay 30 extra euros for extra heating to keep you warm.", answer_accept = "Pay the extra.", answer_reject = "Cold it's just psychological.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 30, happy_consequence_yes = 2, happy_consequence_no = -5, health_consequence_yes = 4, health_consequence_no = -3, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 2, satisfaction_consequence_no = -4},
            new VariableEvents{variable_event = "Had a few extra Spanish classes. Must pay the teacher, muchacho. 20 euros", answer_accept = "Pay it now.", answer_reject = "Pay it +8 euros next week.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 30, immediate_consequence = 20, happy_consequence_yes = 3, happy_consequence_no = -1, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = -3, energy_consequence_no = -3, satisfaction_consequence_yes = 2, satisfaction_consequence_no = -1},
            new VariableEvents{variable_event = "Went to the doctor and you have a vitamine deficiency. Gotta buy some food suplements, but they are costy... 25 euros a box.", answer_accept = "Buy the medication.", answer_reject = "If I eat healthier this deficiency might go away soon enough...", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 25, happy_consequence_yes = -4, happy_consequence_no = -2, health_consequence_yes = 4, health_consequence_no = -8, energy_consequence_yes = 2, energy_consequence_no = -3, satisfaction_consequence_yes = 0, satisfaction_consequence_no = 0},
            new VariableEvents{variable_event = "Your cholesterol is as high as the sky. Must hit the gym and eat better or you are going to have health issues. 50 euros a year plan at the gym.", answer_accept = "Ready to workout!", answer_reject = "Can I avoid a heart attack if I just stop eating fries?", can_happen_multiple_times = false, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 50, happy_consequence_yes = -4, happy_consequence_no = -2, health_consequence_yes = 5, health_consequence_no = -8, energy_consequence_yes = -2, energy_consequence_no = 0, satisfaction_consequence_yes = 0, satisfaction_consequence_no = 0},
            new VariableEvents{variable_event = "Your telephone fell on the pool last weekend and now it doesn't make calls anymore, and you're waiting on a very important call from your boss. Buy a new one for 80 euros.", answer_accept = "Buy a phone.", answer_reject = "I'll put it in rice and it will drain out the problems.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 80, happy_consequence_yes = 3, happy_consequence_no = -3, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = -1, energy_consequence_no = 0, satisfaction_consequence_yes = 2, satisfaction_consequence_no = -4},
            new VariableEvents{variable_event = "The girl scouts are at your door. If you buy a box of cookies you help them to donate to Cancer Research Centers. 10 euros a box.", answer_accept = "Help the people.", answer_reject = "This time I just can't...", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 10, happy_consequence_yes = 2, happy_consequence_no = -2, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 7, satisfaction_consequence_no = -5},
            new VariableEvents{variable_event = "Your cousin has a rare genetic disease. It's time for a whole body check up: blood, tests, endoscopy, heart exams... Gotta make sure everything is fine. But healthcare isn't free, the exams will cost you 55 euros.", answer_accept = "Pay for the exams.", answer_reject = "Gonna leave it as a problem for future me.", can_happen_multiple_times = false, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 55, happy_consequence_yes = -2, happy_consequence_no = -1, health_consequence_yes = 6, health_consequence_no = -5, energy_consequence_yes = -3, energy_consequence_no = 0, satisfaction_consequence_yes = 0, satisfaction_consequence_no = 0},
            new VariableEvents{variable_event = "You're having a lot of backpain lately. Maybe a trip to the chiropractor can make you feel better (And younger). 40 euros a session.", answer_accept = "Go get rid of the pain.", answer_reject = "Must accept that the age comes, and so the body pains.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 40, happy_consequence_yes = 3, happy_consequence_no = -2, health_consequence_yes = 5, health_consequence_no = -6, energy_consequence_yes = 3, energy_consequence_no = -3, satisfaction_consequence_yes = 2, satisfaction_consequence_no = 0},
            new VariableEvents{variable_event = "Oh no! There are moths in the closet! All your pockets now have holes. You can pay 25 euros to fix it.", answer_accept = "Fix the pockets.", answer_reject = "If I fix the pockets, there will be no money left in it.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 25, happy_consequence_yes = -2, happy_consequence_no = -5, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 3, satisfaction_consequence_no = -5},
            new VariableEvents{variable_event = "One of your favorite bands is playing on your town this weekend. Finally, you've waited for so long to see them! But, ouch, tickets are pricy... " + NE_randomInfo[2].st_value + " euros for you and your wife.", answer_accept = "Let's rock!", answer_reject = "Hopefully they'll come again soon...", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = NE_randomInfo[2].int_value, happy_consequence_yes = 7, happy_consequence_no = -3, health_consequence_yes = 2, health_consequence_no = 0, energy_consequence_yes = -4, energy_consequence_no = 0, satisfaction_consequence_yes = 4, satisfaction_consequence_no = -5},
            new VariableEvents{variable_event = "Wow! A promotion: " + NE_randomInfo[3].st_value + " euros for a weekend on a calm beach. Just what you needed after a hard week of work... Right?", answer_accept = "This weekend I'll be under the sun getting tanned.", answer_reject = "I haven't done all the work of the week... Can't take time off.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = NE_randomInfo[3].int_value, happy_consequence_yes = 9, happy_consequence_no = -5, health_consequence_yes = 4, health_consequence_no = 0, energy_consequence_yes = 8, energy_consequence_no = -5, satisfaction_consequence_yes = 5, satisfaction_consequence_no = -6},
            new VariableEvents{variable_event = "Your vacations are comming soon. Maybe that trip to Grece that you and your wife always talk about can finally happen. Just 200 euros.", answer_accept = "Mamma Mia Here We Go!", answer_reject = "If I go to my mom's house during vacations, I can spend zero euros.", can_happen_multiple_times = false, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 200, happy_consequence_yes = 7, happy_consequence_no = -3, health_consequence_yes = 2, health_consequence_no = 0, energy_consequence_yes = -2, energy_consequence_no = 3, satisfaction_consequence_yes = 8, satisfaction_consequence_no = -2},
            new VariableEvents{variable_event = "Jumped on the bed like a child, but forgot that you weight like an adult. The mattres springs are broken. Gotta pay 30 euros to fix it and have nice nights of sleep.", answer_accept = "Fix the springs.", answer_reject = "There's no problem with not sleeping as long I have coffee.", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = 30, happy_consequence_yes = -2, happy_consequence_no = -5, health_consequence_yes = 0, health_consequence_no = -3, energy_consequence_yes = 3, energy_consequence_no = -8, satisfaction_consequence_yes = 0, satisfaction_consequence_no = -1},
            new VariableEvents{variable_event = "Wow. Inflation is back. The prices for coffee are higher now. " + NE_randomInfo[4].st_value + " euros for coffee powder, but what can you do? Coffee is to you what gasoline is for cars...", answer_accept = "Buy coffee anyway.", answer_reject = "I needed to stop that addicition anyway...", can_happen_multiple_times = true, multual_exclusive = 0, late_consequence = 0, immediate_consequence = NE_randomInfo[4].int_value, happy_consequence_yes = -2, happy_consequence_no = -3, health_consequence_yes = 0, health_consequence_no = 1, energy_consequence_yes = 2, energy_consequence_no = -3, satisfaction_consequence_yes = 0, satisfaction_consequence_no = 0}
        };

        //Preparing list of neutral events
        NEut_randomInfo = GetListOfVariableValuesForNeutralEvents(LotteryMoney);
        NeutralEvents = new List<NeutralEventsInfo>(){
            new NeutralEventsInfo{neutral_event = "Feeling lucky today. Why not try the lottery? Just 2 euros a ticket, it can't hurt...", answer_accept = "Let's bet!", answer_reject = "Ignore it.", result_positive = "Yey! Today is your lucky day! You will get " + NEut_randomInfo[0].st_value + " euros next week!", result_negative = "Ouch. Today was not the day... Maybe next time.", immediate_consequence = 2, late_consequence_positive = NEut_randomInfo[0].int_value, happy_consequence_yes_positiveFB = 8, happy_consequence_yes_negativeFB = -3, happy_consequence_no = -1, health_consequence_yes_positiveFB = 0, health_consequence_yes_negativeFB = 0, health_consequence_no = 0, energy_consequence_yes_positiveFB = 0, energy_consequence_yes_negativeFB = 0, energy_consequence_no = 0, satisfaction_consequence_yes_positiveFB = 4, satisfaction_consequence_yes_negativeFB = -1, satisfaction_consequence_no = 0},
            new NeutralEventsInfo{neutral_event = "Found a rat in burger. If you pay 50 euros for a lawyer this week, you can gain 150 euros next week if you win a lawsuit against the restaurant.", answer_accept = "To the courts.", answer_reject = "Can't pay the lawyer right now.", result_positive = "Yey! Your lawyer rocked in court. Thanks for the rat!", result_negative = "Well...It didn't work out, but at least you saw the rat before eating it.", immediate_consequence = 50, late_consequence_positive = 150, happy_consequence_yes_positiveFB = 6, happy_consequence_yes_negativeFB = -5, happy_consequence_no = -3, health_consequence_yes_positiveFB = -3, health_consequence_yes_negativeFB = -3, health_consequence_no = -3, energy_consequence_yes_positiveFB = -2, energy_consequence_yes_negativeFB = 0, energy_consequence_no = 0, satisfaction_consequence_yes_positiveFB = -1, satisfaction_consequence_yes_negativeFB = -8, satisfaction_consequence_no = -4},
            new NeutralEventsInfo{neutral_event = "Two colleagues been hanging around too much lately. A friend wants to bet that they are in a secret relationship. 5 euros to bet, and maybe win 10 euros if you're right.", answer_accept = "I bet on their love.", answer_reject = "What a shame to bet on people's back like that.", result_positive = "Yey! Love is in the air and profit as well.", result_negative = "Not everything is what it seems afterall.", immediate_consequence = 5, late_consequence_positive = 10, happy_consequence_yes_positiveFB = 2, happy_consequence_yes_negativeFB = -2, happy_consequence_no = -1, health_consequence_yes_positiveFB = 0, health_consequence_yes_negativeFB = 0, health_consequence_no = 0, energy_consequence_yes_positiveFB = 0, energy_consequence_yes_negativeFB = 0, energy_consequence_no = 0, satisfaction_consequence_yes_positiveFB = 3, satisfaction_consequence_yes_negativeFB = -2, satisfaction_consequence_no = 0}
        };

        //Preparing list of dependent events
        DependentEvents = new List<DependentEventsInfo>(){
            new DependentEventsInfo{dependent_event = "Wow, how nice! You're going to be a daddy! Gotta make sure the baby is healthy. The prenatal exams costs 250 euros.", answer_accept = "Pay it.", answer_reject = "Have faith that everything is fine with the baby.", can_happen_multiple_times = false, there_is_dependency = false, immediate_consequence = 250, happy_consequence_yes = 6, happy_consequence_no = -4, health_consequence_yes = 0, health_consequence_no = -2, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 3, satisfaction_consequence_no = -6},
            new DependentEventsInfo{dependent_event = "Must prepare for the baby, it will need a lot of diapers, better start stocking. 15 euros a package.", answer_accept = "Buy it.", answer_reject = "Are diapers really necessary?", can_happen_multiple_times = true, there_is_dependency = true, immediate_consequence = 15, happy_consequence_yes = 2, happy_consequence_no = -4, health_consequence_yes = 0, health_consequence_no = -2, energy_consequence_yes = -1, energy_consequence_no = 0, satisfaction_consequence_yes = 1, satisfaction_consequence_no = -6},
            new DependentEventsInfo{dependent_event = "Now that the family is growing you need a safer car. At least the dealership offered you a good plan: pay 200 euros this month and start paying the next fees only next year. Are you going to keep the baby safe?", answer_accept = "Buy car.", answer_reject = "The seatbell is good enough.", can_happen_multiple_times = false, there_is_dependency = true, immediate_consequence = 200, happy_consequence_yes = 4, happy_consequence_no = -4, health_consequence_yes = 0, health_consequence_no = -2, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 3, satisfaction_consequence_no = -6},
            new DependentEventsInfo{dependent_event = "Aren't you curious to know the sex of your baby? 30 euros for the exam.", answer_accept = "Pay it.", answer_reject = "Patience is a blessing.", can_happen_multiple_times = false, there_is_dependency = true, immediate_consequence = 30, happy_consequence_yes = 2, happy_consequence_no = -1, health_consequence_yes = 0, health_consequence_no = 0, energy_consequence_yes = 0, energy_consequence_no = 0, satisfaction_consequence_yes = 3, satisfaction_consequence_no = -6}
        };


        /*
        OnAccountChange.Invoke (0, 100);
        int happyyyyyy = 10;
        OnHappinessChange.Invoke(happyyyyyy);
        RealGameManager.health=10+RealGameManager.health;
        */
        
    }

    //----------------------------------------------------------------------------------------------------------//

    //---------------------------------------------****GAME FLOW****------------------------------------------//


    public List<EventsSent> EventRequirement_(bool consider_minigame_scores, string minigame_effort, float minigame_score){
        List<EventsSent> Events_Of_The_Week;

        if (consider_minigame_scores == true){
            if(minigame_effort == "easy"){
                bonus_payment = minigame_score*25;
                effort = 1;
            }
            else{
                if (minigame_effort == "medium"){
                    bonus_payment = minigame_score*50;
                    effort = 2;
                }
                else{
                    bonus_payment = minigame_score*100;
                    effort = 3;
                }
            }
            st_bonus_payment = bonus_payment.ToString();
        }

        Events_Of_The_Week = this_week(week, PositiveEventsIndexes, NegativeEventsIndexes, NeutralEventsIndexes, DependentEventsIndexes); 
        return Events_Of_The_Week;
    }

    public changes_this_week BudgetScoreChangesRequirement_(List<int> choices_player){
        return_parameters impacts_of_the_week;
        float budget_change_of_the_week;
        int happy_change_of_the_week;
        int health_change_of_the_week;
        int energy_change_of_the_week;
        int socialSatisfaction_change_of_the_week;
        int feedbacks_int;
        List<string> feedback_list;

        impacts_of_the_week = updating_budget(week, budget_update_following_week, choices_player);
        budget_change_of_the_week = impacts_of_the_week.budget_week_i;
        budget_update_following_week = impacts_of_the_week.budget_week_i_plus_1;
        happy_change_of_the_week = impacts_of_the_week.happy_week_i;
        health_change_of_the_week = impacts_of_the_week.health_week_i;
        energy_change_of_the_week = impacts_of_the_week.energy_week_i;
        socialSatisfaction_change_of_the_week = impacts_of_the_week.socialSatisfaction_week_i;
        feedbacks_int = impacts_of_the_week.feedback_count;
        feedback_list = impacts_of_the_week.feedback;


        if (week == 4){
            if (month == 6){
                month = 0;
                PositiveEventsIndexes = new List<int> {0,1,2,3,4,5};
                NegativeEventsIndexes = new List<int> {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
                NeutralEventsIndexes = new List<int> {0,1,2};
                DependentEventsIndexes = new List<int> {0,1,2,3};
                wife_pregnant = false;
                control_1 = false;
                control_2 = false;
                TypeOfVariableQuestion = new List<int> {0,1,2,3};
            }
            new_randoms(true);
            month = month + 1;
            week = 0;
        }
        else{
            new_randoms(false);
        }
        week = week + 1;


        changes_this_week week_changes = new changes_this_week{budget_week_i = budget_change_of_the_week, happy_week_i = happy_change_of_the_week, health_week_i = health_change_of_the_week, energy_week_i = energy_change_of_the_week, socialSatisfaction_week_i = socialSatisfaction_change_of_the_week, feedback_count = feedbacks_int, feedback = feedback_list};
        return week_changes;
    }

    public void ReStartGame_(){
        month = 1;
        week = 1;
        PositiveEventsIndexes = new List<int> {0,1,2,3,4,5};
        NegativeEventsIndexes = new List<int> {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
        NeutralEventsIndexes = new List<int> {0,1,2};
        DependentEventsIndexes = new List<int> {0,1,2,3};
        wife_pregnant = false;
        control_1 = false;
        control_2 = false;
        TypeOfVariableQuestion = new List<int> {0,1,2,3};
        budget_update_following_week = 0;
        effort = 0;
        new_randoms(true);
    }

    //----------------------------------------------------------------------------------------------------------//


    //---------------------------------------------****FUNCTIONS****--------------------------------------------//

    //Function to get a random element of a list
    private int GetRandomValue(List<int> list_of_values){
        int index = Random.Range(0, (list_of_values.Count));
        return list_of_values[index];
    }

    //Function to get the random values existing in the Fixed Events
    private List<VariableInfo> GetListOfVariableValuesForFixedEvents(List<int> list_connection_bills,List<int> list_utilities_bills, List<int> list_groceries_bills){
        int pay_connection = GetRandomValue(ConnectionBills);
        int pay_utilities = GetRandomValue(UtilitiesBills);
        int pay_groceries = GetRandomValue(GroceriesBills);
        string st_pay_connection = pay_connection.ToString();
        string st_pay_utilities = pay_utilities.ToString();
        string st_pay_groceries = pay_groceries.ToString();

        List<VariableInfo> list_with_info_FE = new List<VariableInfo>(){
            new VariableInfo(pay_connection, st_pay_connection),
            new VariableInfo(pay_utilities, st_pay_utilities),
            new VariableInfo(pay_groceries, st_pay_groceries)
        };

        return list_with_info_FE;
    }

    //Function to get the random values existing in the Positive Events
    private List<VariableInfo> GetListOfVariableValuesForPositiveEvents(List<int> list_of_euros_found, List<int> list_of_heritage_gain, List<int> list_of_insurance_gain, List<int> list_of_dog_pict){
        int rd = GetRandomValue(list_of_euros_found);
        string st_rd = rd.ToString();
        List <VariableInfo> list_with_info_PE = new List<VariableInfo>(){
            new VariableInfo(rd, st_rd)
        };
        rd = GetRandomValue(list_of_heritage_gain);
        st_rd = rd.ToString();
        list_with_info_PE.Add(new VariableInfo(rd, st_rd));
        rd = GetRandomValue(list_of_euros_found);
        st_rd = rd.ToString();
        list_with_info_PE.Add(new VariableInfo(rd, st_rd));
        rd = GetRandomValue(list_of_insurance_gain);
        st_rd = rd.ToString();
        list_with_info_PE.Add(new VariableInfo(rd, st_rd));
        rd = GetRandomValue(list_of_dog_pict);
        st_rd = rd.ToString();
        list_with_info_PE.Add(new VariableInfo(rd, st_rd));

        return list_with_info_PE;
        
    }

    //Function to get the random values existing in the Negative Events
    private List<VariableInfo> GetListOfVariableValuesForNegativeEvents(List<int> list_euros_lent, List<int> list_euros_catch_up, List<int> list_euros_show, List<int> list_euros_beach_promotion, List<int> list_euros_coffee){
        int rd = GetRandomValue(list_euros_lent);
        string st_rd = rd.ToString();
        List<VariableInfo> list_with_info_NE = new List<VariableInfo>(){
            new VariableInfo(rd, st_rd)
        };
        rd = GetRandomValue(list_euros_catch_up);
        st_rd = rd.ToString();
        list_with_info_NE.Add(new VariableInfo(rd, st_rd));
        rd = GetRandomValue(list_euros_show);
        st_rd = rd.ToString();
        list_with_info_NE.Add(new VariableInfo(rd, st_rd));
        rd = GetRandomValue(list_euros_beach_promotion);
        st_rd = rd.ToString();
        list_with_info_NE.Add(new VariableInfo(rd, st_rd));
        rd = GetRandomValue(list_euros_coffee);
        st_rd = rd.ToString();
        list_with_info_NE.Add(new VariableInfo(rd, st_rd));

        return list_with_info_NE;
    }

    //Function to get the random values existing in the Neutral Events
    private List<VariableInfo> GetListOfVariableValuesForNeutralEvents(List<int> list_of_euros_lottery){
        int rd = GetRandomValue(list_of_euros_lottery);
        string st_rd = rd.ToString();
        List<VariableInfo> list_with_info_NEut = new List<VariableInfo>(){
            new VariableInfo(rd, st_rd)
        };

        return list_with_info_NEut;
    }


    //What events are going to take place in week 1 this month?
    private List<EventsSent> this_week(int current_week, List<int> PE_indexes_global, List<int> NE_indexes_global, List<int> NEut_indexes_global, List<int> Dep_indexes_global){

        int number_of_questions = GetRandomValue(NumberOfVariableQuestions);
        List<EventsSent> events_selection = new List<EventsSent>();
        QuestionsChosenType = new List<int>();
        QuestionsChosenIndexes = new List<int>();
        List<int> PE_indexes = new List<int>();
        List<int> NE_indexes = new List<int>();
        List<int> NEut_indexes = new List<int>();
        List<int> Dep_indexes = new List<int>();
        for(int i = 0; i < PE_indexes_global.Count; i++){
            PE_indexes.Add(PE_indexes_global[i]);
        }
        for(int i = 0; i < NE_indexes_global.Count; i++){
            NE_indexes.Add(NE_indexes_global[i]);
        }
        for(int i = 0; i < NEut_indexes_global.Count; i++){
            NEut_indexes.Add(NEut_indexes_global[i]);
        }
        for(int i = 0; i < Dep_indexes_global.Count; i++){
            Dep_indexes.Add(Dep_indexes_global[i]);
        }

        if(current_week == 1){
            FixedEvents[1].fixed_event = "Here's your bonus for all the hard work! +" + st_bonus_payment + " euros for you.";

            events_selection.Add(new EventsSent{event_text = FixedEvents[0].fixed_event, answer_1 = ".", answer_2 = ".", event_type = 0});
            events_selection.Add(new EventsSent{event_text = FixedEvents[1].fixed_event, answer_1 = ".", answer_2 = ".", event_type = 0});
            events_selection.Add(new EventsSent{event_text = FixedEvents[5].fixed_event, answer_1 = ".", answer_2 = ".", event_type = 0});
        }
        else{
            if(current_week == 2){
                events_selection.Add(new EventsSent{event_text = FixedEvents[4].fixed_event, answer_1 = ".", answer_2 = ".", event_type = 0});
            }
            else{
                if (current_week == 3){
                    events_selection.Add(new EventsSent{event_text = FixedEvents[2].fixed_event, answer_1 = ".", answer_2 = ".", event_type = 0});
                }
                else{
                    if(current_week == 4){
                        events_selection.Add(new EventsSent{event_text = FixedEvents[3].fixed_event, answer_1 = ".", answer_2 = ".", event_type = 0});
                    }
                }
            }
        }

        for (int i = 0; i < number_of_questions; i++)
        {
            if((NEut_indexes.Count == 0) & (control_1 == false)){
                TypeOfVariableQuestion.Remove(2);
                control_1 = true;
            }
            if((Dep_indexes.Count == 0) & (control_2 == false)){
                TypeOfVariableQuestion.Remove(3);
                control_2 = true;
            }
            int type = GetRandomValue(TypeOfVariableQuestion);
            QuestionsChosenType.Add(type);
            if (type == 0){
                int question = GetRandomValue(PE_indexes);
                QuestionsChosenIndexes.Add(question);
                events_selection.Add(new EventsSent{event_text = PositiveEvents[question].variable_event, answer_1 = PositiveEvents[question].answer_accept, answer_2 = PositiveEvents[question].answer_reject, event_type = 1});
                PE_indexes.Remove(question);
                if (PositiveEvents[question].can_happen_multiple_times == false){
                    PositiveEventsIndexes.Remove(question);
                }
                if (PositiveEvents[question].multual_exclusive == 12){
                    NegativeEventsIndexes.Remove(0);
                }
                if (PositiveEvents[question].multual_exclusive == 16){
                    NegativeEventsIndexes.Remove(4);
                }
            }
            else{
                if (type == 1){
                    int question = GetRandomValue(NE_indexes);
                    QuestionsChosenIndexes.Add(question);
                    events_selection.Add(new EventsSent{event_text = NegativeEvents[question].variable_event, answer_1 = NegativeEvents[question].answer_accept, answer_2 = NegativeEvents[question].answer_reject, event_type = 1});
                    NE_indexes.Remove(question);
                    if (NegativeEvents[question].can_happen_multiple_times == false){
                        NegativeEventsIndexes.Remove(question);
                    }
                    if (NegativeEvents[question].multual_exclusive == 8){
                        PositiveEventsIndexes.Remove(2);
                    }
                    if (NegativeEvents[question].multual_exclusive == 10){
                        PositiveEventsIndexes.Remove(4);
                    }
                }
                else{
                    if (type == 2){
                        int question = GetRandomValue(NEut_indexes);
                        QuestionsChosenIndexes.Add(question);
                        events_selection.Add(new EventsSent{event_text = NeutralEvents[question].neutral_event, answer_1 = NeutralEvents[question].answer_accept, answer_2 = NeutralEvents[question].answer_reject, event_type = 2});
                        NEut_indexes.Remove(question);
                        //new part
                        NeutralEventsIndexes.Remove(question);
                    }
                    else{
                        if (wife_pregnant == false){
                            QuestionsChosenIndexes.Add(0);
                            events_selection.Add(new EventsSent{event_text = DependentEvents[0].dependent_event, answer_1 = DependentEvents[0].answer_accept, answer_2 = DependentEvents[0].answer_reject, event_type = 3});
                            Dep_indexes.Remove(0);
                            DependentEventsIndexes.Remove(0);
                            wife_pregnant = true;
                        }
                        else{
                            int question = GetRandomValue(Dep_indexes);
                            QuestionsChosenIndexes.Add(question);
                            events_selection.Add(new EventsSent{event_text = DependentEvents[question].dependent_event, answer_1 = DependentEvents[question].answer_accept, answer_2 = DependentEvents[question].answer_reject, event_type = 3});
                            Dep_indexes.Remove(question);
                            //new part
                            DependentEventsIndexes.Remove(question);
                            /*
                            if (DependentEvents[question].can_happen_multiple_times == false){
                                DependentEventsIndexes.Remove(question);
                            }
                            */
                        }
                    }
                }   
            }
        }

        return events_selection;
    }

    //What is going to happen with the budget this week (and in the next) after player's choices? 
    private return_parameters updating_budget(int current_week, float previous_calculation_budget, List<int> choices_player){
        float budget_variation = previous_calculation_budget;
        float budget_variation_next_week = 0;
        int happy_variation = 0;
        int health_variation = 0;
        int energy_variation = 0;
        int satisfaction_variation = 0;
        List<string> feedback_for_neutral_events = new List<string>();

        if (current_week == 1){
            //sallary and bonus; minus supermarket;
            budget_variation = budget_variation + 1500 + bonus_payment - FixedEvents[5].immediate_consequence;
            happy_variation = happy_variation + 10;
            if(effort == 1){
                energy_variation = energy_variation + 9;
            }
            else{
                if (effort == 2){
                    energy_variation = energy_variation + 4;
                }
                else{
                    if (effort == 3){
                        energy_variation = energy_variation -7;
                    }
                }
            }
        }
        else{
            happy_variation = happy_variation -5;
            if(current_week == 2){
                //minus water bill
                budget_variation = budget_variation - FixedEvents[4].immediate_consequence;
            }
            else{
                //minus internet bill
                if(current_week == 3){
                    budget_variation = budget_variation - FixedEvents[2].immediate_consequence;
                }
                else{
                    //minus rent
                    budget_variation = budget_variation -700;
                }
            }
        }
        for (int i = 0; i < choices_player.Count; i++){
            if(QuestionsChosenType[i] == 0){
                if (choices_player[i] == 0){
                    budget_variation = budget_variation + PositiveEvents[QuestionsChosenIndexes[i]].immediate_consequence;
                    happy_variation = happy_variation + PositiveEvents[QuestionsChosenIndexes[i]].happy_consequence_yes;
                    health_variation = health_variation + PositiveEvents[QuestionsChosenIndexes[i]].health_consequence_yes;
                    energy_variation = energy_variation + PositiveEvents[QuestionsChosenIndexes[i]].energy_consequence_yes;
                    satisfaction_variation = satisfaction_variation + PositiveEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_yes;
                }
                else{
                    happy_variation = happy_variation + PositiveEvents[QuestionsChosenIndexes[i]].happy_consequence_no;
                    health_variation = health_variation + PositiveEvents[QuestionsChosenIndexes[i]].health_consequence_no;
                    energy_variation = energy_variation + PositiveEvents[QuestionsChosenIndexes[i]].energy_consequence_no;
                    satisfaction_variation = satisfaction_variation + PositiveEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_no;
                }
            }
            else{
                if(QuestionsChosenType[i] == 1){
                    if(choices_player[i] == 0){
                        budget_variation = budget_variation - NegativeEvents[QuestionsChosenIndexes[i]].immediate_consequence;
                        happy_variation = happy_variation + NegativeEvents[QuestionsChosenIndexes[i]].happy_consequence_yes;
                        health_variation = health_variation + NegativeEvents[QuestionsChosenIndexes[i]].health_consequence_yes;
                        energy_variation = energy_variation + NegativeEvents[QuestionsChosenIndexes[i]].energy_consequence_yes;
                        satisfaction_variation = satisfaction_variation + NegativeEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_yes;
                    }
                    else{
                        budget_variation_next_week = budget_variation_next_week - NegativeEvents[QuestionsChosenIndexes[i]].late_consequence;
                        happy_variation = happy_variation + NegativeEvents[QuestionsChosenIndexes[i]].happy_consequence_no;
                        health_variation = health_variation + NegativeEvents[QuestionsChosenIndexes[i]].health_consequence_no;
                        energy_variation = energy_variation + NegativeEvents[QuestionsChosenIndexes[i]].energy_consequence_no;
                        satisfaction_variation = satisfaction_variation + NegativeEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_no;
                    }
                }
                else{
                    if(QuestionsChosenType[i] == 2){
                        if (choices_player[i] == 0){
                            budget_variation = budget_variation - NeutralEvents[QuestionsChosenIndexes[i]].immediate_consequence;

                            int random_feedback = GetRandomValue(BinaryVariables);
                            if (random_feedback == 1){
                                budget_variation_next_week = budget_variation_next_week + NeutralEvents[QuestionsChosenIndexes[i]].late_consequence_positive;
                                //Debug.Log("uhu, you won something random");
                                feedback_for_neutral_events.Add(NeutralEvents[QuestionsChosenIndexes[i]].result_positive);
                                happy_variation = happy_variation + NeutralEvents[QuestionsChosenIndexes[i]].happy_consequence_yes_positiveFB;
                                health_variation = health_variation + NeutralEvents[QuestionsChosenIndexes[i]].health_consequence_yes_positiveFB;
                                energy_variation = energy_variation + NeutralEvents[QuestionsChosenIndexes[i]].energy_consequence_yes_positiveFB;
                                satisfaction_variation = satisfaction_variation + NeutralEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_yes_positiveFB;
                            }
                            else{
                                //Debug.Log("to bad 4 u");
                                feedback_for_neutral_events.Add(NeutralEvents[QuestionsChosenIndexes[i]].result_negative);
                                happy_variation = happy_variation + NeutralEvents[QuestionsChosenIndexes[i]].happy_consequence_yes_negativeFB;
                                health_variation = health_variation + NeutralEvents[QuestionsChosenIndexes[i]].health_consequence_yes_negativeFB;
                                energy_variation = energy_variation + NeutralEvents[QuestionsChosenIndexes[i]].energy_consequence_yes_negativeFB;
                                satisfaction_variation = satisfaction_variation + NeutralEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_yes_negativeFB;
                            }
                        }
                        else{
                            happy_variation = happy_variation + NeutralEvents[QuestionsChosenIndexes[i]].happy_consequence_no;
                            health_variation = health_variation + NeutralEvents[QuestionsChosenIndexes[i]].health_consequence_no;
                            energy_variation = energy_variation + NeutralEvents[QuestionsChosenIndexes[i]].energy_consequence_no;
                            satisfaction_variation = satisfaction_variation + NeutralEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_no;
                        }
                    }
                    else{
                        if(choices_player[i] == 0){
                            budget_variation = budget_variation - DependentEvents[QuestionsChosenIndexes[i]].immediate_consequence;
                            happy_variation = happy_variation + DependentEvents[QuestionsChosenIndexes[i]].happy_consequence_yes;
                            health_variation = health_variation + DependentEvents[QuestionsChosenIndexes[i]].health_consequence_yes;
                            energy_variation = energy_variation + DependentEvents[QuestionsChosenIndexes[i]].energy_consequence_yes;
                            satisfaction_variation = satisfaction_variation + DependentEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_yes;
                        }
                        else{
                            happy_variation = happy_variation + DependentEvents[QuestionsChosenIndexes[i]].happy_consequence_no;
                            health_variation = health_variation + DependentEvents[QuestionsChosenIndexes[i]].health_consequence_no;
                            energy_variation = energy_variation + DependentEvents[QuestionsChosenIndexes[i]].energy_consequence_no;
                            satisfaction_variation = satisfaction_variation + DependentEvents[QuestionsChosenIndexes[i]].satisfaction_consequence_no;
                        }
                    }
                }
            }
        }
        return_parameters week_results = new return_parameters{budget_week_i = budget_variation, budget_week_i_plus_1 = budget_variation_next_week, happy_week_i = happy_variation, health_week_i = health_variation, energy_week_i = energy_variation, socialSatisfaction_week_i = satisfaction_variation, feedback_count = feedback_for_neutral_events.Count, feedback = feedback_for_neutral_events};
        return week_results;
    }

    //Get new random values for the events.
    private void new_randoms(bool update_fixed_too){
        if (update_fixed_too == true){
            FE_randomInfo = GetListOfVariableValuesForFixedEvents(ConnectionBills, UtilitiesBills, GroceriesBills);
            FixedEvents[2].fixed_event = "Electricity, cable TV, Internet... Gotta pay the bills to stay connected. Must pay " + FE_randomInfo[0].st_value + " euros.";
            FixedEvents[2].immediate_consequence = FE_randomInfo[0].int_value;
            FixedEvents[4].fixed_event = "Water and gaz bills arrived. Looks like natural ressources ain't free afterall. What a bummer! Must pay " + FE_randomInfo[1].st_value + " euros.";
            FixedEvents[4].immediate_consequence = FE_randomInfo[1].int_value;
            FixedEvents[5].fixed_event = "Went to the supermarket for groceries and essentials buys. A guy gotta eat... Must pay now " + FE_randomInfo[2].st_value + " euros.";
            FixedEvents[5].immediate_consequence = FE_randomInfo[2].int_value;
        }


        PE_randomInfo = GetListOfVariableValuesForPositiveEvents(EurosFound_Borrowed, HeritageObtained, InsuranceObtained, PictOfDog);
        PositiveEvents[0].variable_event = "Wow. Look, there's " + PE_randomInfo[0].st_value + " euros in the street! Do you take it? You kinda shouldn't...";
        PositiveEvents[0].immediate_consequence = PE_randomInfo[0].int_value;
        PositiveEvents[2].variable_event = "Oh no, your not so distant aunt Beth has passed away. But, since you were the sweetest nephew, aunty left you " + PE_randomInfo[1].st_value + " euros as heritage.";
        PositiveEvents[2].immediate_consequence = PE_randomInfo[1].int_value;
        PositiveEvents[3].variable_event = "A friend owes you " + PE_randomInfo[2].st_value + " euros. Ask him for the money? He has been so upset lately...";
        PositiveEvents[3].immediate_consequence = PE_randomInfo[2].int_value;
        PositiveEvents[4].variable_event = "Oh, no. You were ran over by an ice-cream truck. At least insurance will pay you " + PE_randomInfo[3].st_value + " euros and you didn't break any bones.";
        PositiveEvents[4].immediate_consequence = PE_randomInfo[3].int_value;
        PositiveEvents[5].variable_event = "Sold a cool picture of your dog to the local newspaper. Here's " + PE_randomInfo[4].st_value + " euros for your contribution.";
        PositiveEvents[5].immediate_consequence = PE_randomInfo[4].int_value;

        NE_randomInfo = GetListOfVariableValuesForNegativeEvents(EurosFound_Borrowed, CatchUpFriend, ShowTickets, BeachPromotion, CoffeeMoney);
        NegativeEvents[1].variable_event = "You owe " + NE_randomInfo[0].st_value +  " euros to a friend. Pay him?";
        NegativeEvents[1].immediate_consequence = NE_randomInfo[0].int_value;
        NegativeEvents[8].variable_event = "An old friend is in town and she would love to go out to catch up. Just " + NE_randomInfo[1].st_value + " euros to revisit old times...";
        NegativeEvents[8].immediate_consequence = NE_randomInfo[1].int_value;
        NegativeEvents[19].variable_event = "One of your favorite bands is playing on your town this weekend. Finally, you've waited for so long to see them! But, ouch, tickets are pricy... " + NE_randomInfo[2].st_value + " euros for you and your wife.";
        NegativeEvents[19].immediate_consequence = NE_randomInfo[2].int_value;
        NegativeEvents[20].variable_event = "Wow! A promotion: " + NE_randomInfo[3].st_value + " euros for a weekend on a calm beach. Just what you needed after a hard week of work... Right?";
        NegativeEvents[20].immediate_consequence = NE_randomInfo[3].int_value;
        NegativeEvents[23].variable_event = "Wow. Inflation is back. The prices for coffee are higher now. " + NE_randomInfo[4].st_value + " euros for coffe powder, but what can you do? Coffee is to you what gasoline is for cars...";
        NegativeEvents[23].immediate_consequence = NE_randomInfo[4].int_value;
    
        NEut_randomInfo =  GetListOfVariableValuesForNeutralEvents(LotteryMoney);
        NeutralEvents[0].result_positive = "Yey! Today is your lucky day! You will get " + NEut_randomInfo[0].st_value + " euros next week!";
        NeutralEvents[0].late_consequence_positive =  NEut_randomInfo[0].int_value;

    }

    //----------------------------------------------------------------------------------------------------------//

    //---------------------------------------------****CLASSES DEFINITION****--------------------------------------------//
    
    //class for the values that are randomly chosen in the events description
    private class VariableInfo{
        public int int_value;
        public string st_value;

        public VariableInfo (int i, string s)
        {
            int_value = i;
            st_value = s;

          }  
    }

    //class to define the fixed events
    private class FixedEventsInfo{
        public string fixed_event;
        public float immediate_consequence;
    }

    //class to define the positive and negative events
    private class VariableEvents{
        public string variable_event;
        public string answer_accept;
        public string answer_reject;
        public bool can_happen_multiple_times;
        public int multual_exclusive;
        public int late_consequence;
        public int immediate_consequence;
        public int happy_consequence_yes;
        public int happy_consequence_no;
        public int health_consequence_yes;
        public int health_consequence_no;
        public int energy_consequence_yes;
        public int energy_consequence_no;
        public int satisfaction_consequence_yes;
        public int satisfaction_consequence_no;
    }

    //class to define neutral events
    private class NeutralEventsInfo{
        public string neutral_event;
        public string answer_accept;
        public string answer_reject;
        public string result_positive;
        public string result_negative;
        public int immediate_consequence;
        public int late_consequence_positive;
        public int happy_consequence_yes_positiveFB;
        public int happy_consequence_yes_negativeFB;
        public int happy_consequence_no;
        public int health_consequence_yes_positiveFB;
        public int health_consequence_yes_negativeFB;
        public int health_consequence_no;
        public int energy_consequence_yes_positiveFB;
        public int energy_consequence_yes_negativeFB;
        public int energy_consequence_no;
        public int satisfaction_consequence_yes_positiveFB;
        public int satisfaction_consequence_yes_negativeFB;
        public int satisfaction_consequence_no;
    }

    //class to define dependent events
    private class DependentEventsInfo{
        public string dependent_event;
        public string answer_accept;
        public string answer_reject;
        public bool can_happen_multiple_times;
        public bool there_is_dependency;
        public int immediate_consequence;
        public int happy_consequence_yes;
        public int happy_consequence_no;
        public int health_consequence_yes;
        public int health_consequence_no;
        public int energy_consequence_yes;
        public int energy_consequence_no;
        public int satisfaction_consequence_yes;
        public int satisfaction_consequence_no;
    }

    //class to prepare the information sent to game manager//events sent for the player
    public class EventsSent{
        public string event_text;
        public string answer_1;
        public string answer_2;
        //fixed event = 0; variable event = 1;
        public int event_type;
    }

    //class to prepare the information sent to game manager//impact of player's choices
    private class return_parameters{
        public float budget_week_i;
        public float budget_week_i_plus_1;  
        public int happy_week_i;
        public int health_week_i;
        public int energy_week_i;
        public int socialSatisfaction_week_i;
        public int feedback_count;
        public List<string> feedback;
    } 

    public class changes_this_week{
        public float budget_week_i;  
        public int happy_week_i;
        public int health_week_i;
        public int energy_week_i;
        public int socialSatisfaction_week_i;
        public int feedback_count;
        public List<string> feedback;
    } 


}
